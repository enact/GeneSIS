#include <stdint.h>
#include <Arduino.h>
/* Adds and instance to the runtime and returns its id */
uint16_t add_instance(void * instance_struct);
/* Returns the instance with id */
void * instance_by_id(uint16_t id);

/* Returns the number of byte currently in the fifo */
int fifo_byte_length();
/* Returns the number of bytes currently available in the fifo */
int fifo_byte_available();
/* Returns true if the fifo is empty */
int fifo_empty();
/* Return true if the fifo is full */
int fifo_full();
/* Enqueue 1 byte in the fifo if there is space
   returns 1 for sucess and 0 if the fifo was full */
int fifo_enqueue(byte b);
/* Enqueue 1 byte in the fifo without checking for available space
   The caller should have checked that there is enough space */
int _fifo_enqueue(byte b);
/* Dequeue 1 byte in the fifo.
   The caller should check that the fifo is not empty */
byte fifo_dequeue();
/*****************************************************************************
 * Headers for type : GatewayCommandParser
 *****************************************************************************/

// Definition of the instance struct:
struct GatewayCommandParser_Instance {

// Instances of different sessions
bool active;
// Variables for the ID of the ports of the instance
uint16_t id_blerx;
uint16_t id_bletx;
uint16_t id_gateway;
// Variables for the current instance state
int GatewayCommandParser_GatewayCommandParserSC_State;
// Variables for the properties of the instance

};
// Declaration of prototypes outgoing messages :
void GatewayCommandParser_GatewayCommandParserSC_OnEntry(int state, struct GatewayCommandParser_Instance *_instance);
void GatewayCommandParser_handle_blerx_receive_byte(struct GatewayCommandParser_Instance *_instance, uint8_t b);
void GatewayCommandParser_handle_gateway_button_pushed(struct GatewayCommandParser_Instance *_instance);
// Declaration of callbacks for incoming messages:
void register_GatewayCommandParser_send_bletx_write_byte_listener(void (*_listener)(struct GatewayCommandParser_Instance *, uint8_t));
void register_external_GatewayCommandParser_send_bletx_write_byte_listener(void (*_listener)(struct GatewayCommandParser_Instance *, uint8_t));
void register_GatewayCommandParser_send_bletx_print_message_listener(void (*_listener)(struct GatewayCommandParser_Instance *, char *));
void register_external_GatewayCommandParser_send_bletx_print_message_listener(void (*_listener)(struct GatewayCommandParser_Instance *, char *));
void register_GatewayCommandParser_send_gateway_clear_screen_listener(void (*_listener)(struct GatewayCommandParser_Instance *));
void register_external_GatewayCommandParser_send_gateway_clear_screen_listener(void (*_listener)(struct GatewayCommandParser_Instance *));
void register_GatewayCommandParser_send_gateway_display_digit_listener(void (*_listener)(struct GatewayCommandParser_Instance *, int8_t));
void register_external_GatewayCommandParser_send_gateway_display_digit_listener(void (*_listener)(struct GatewayCommandParser_Instance *, int8_t));

// Definition of the states:
#define GATEWAYCOMMANDPARSER_GATEWAYCOMMANDPARSERSC_STATE 0
#define GATEWAYCOMMANDPARSER_GATEWAYCOMMANDPARSERSC_READY_STATE 1


/*****************************************************************************
 * Headers for type : DisplayArduino
 *****************************************************************************/


// BEGIN: Code from the c_header annotation DisplayArduino

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>

#define TFT_CS     10
#define TFT_RST    9
#define TFT_DC     8
// END: Code from the c_header annotation DisplayArduino

// Definition of the instance struct:
struct DisplayArduino_Instance {

// Instances of different sessions
bool active;
// Variables for the ID of the ports of the instance
uint16_t id_display;
// Variables for the current instance state
int Display_SC_State;
// Variables for the properties of the instance
uint8_t Display_fg_g_var;
uint16_t DisplayArduino_color_var;
uint16_t DisplayArduino_YFRAMESIZE_var;
uint8_t Display_bg_r_var;
uint8_t Display_bg_b_var;
uint8_t Display_bg_g_var;
uint16_t DisplayArduino_XFRAMESIZE_var;
uint8_t Display_fg_r_var;
uint8_t Display_fg_b_var;

};
// Declaration of prototypes outgoing messages :
void Display_SC_OnEntry(int state, struct DisplayArduino_Instance *_instance);
void DisplayArduino_handle_display_drawInteger(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, int16_t v, uint8_t digits, uint8_t scale);
void DisplayArduino_handle_display_create(struct DisplayArduino_Instance *_instance, uint8_t xsize, uint8_t ysize);
void DisplayArduino_handle_display_fillRect(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height);
void DisplayArduino_handle_display_update(struct DisplayArduino_Instance *_instance);
void DisplayArduino_handle_display_setBGColor(struct DisplayArduino_Instance *_instance, uint8_t r, uint8_t g, uint8_t b);
void DisplayArduino_handle_display_drawThingML(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y);
void DisplayArduino_handle_display_setColor(struct DisplayArduino_Instance *_instance, uint8_t r, uint8_t g, uint8_t b);
void DisplayArduino_handle_display_destroy(struct DisplayArduino_Instance *_instance);
void DisplayArduino_handle_display_clear(struct DisplayArduino_Instance *_instance);
void DisplayArduino_handle_display_drawRect(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height);
// Declaration of callbacks for incoming messages:
void register_DisplayArduino_send_display_displayReady_listener(void (*_listener)(struct DisplayArduino_Instance *));
void register_external_DisplayArduino_send_display_displayReady_listener(void (*_listener)(struct DisplayArduino_Instance *));
void register_DisplayArduino_send_display_displayError_listener(void (*_listener)(struct DisplayArduino_Instance *));
void register_external_DisplayArduino_send_display_displayError_listener(void (*_listener)(struct DisplayArduino_Instance *));

// Definition of the states:
#define DISPLAY_SC_STATE 0
#define DISPLAY_SC_WAIT_STATE 1
#define DISPLAY_SC_RUNNING_STATE 2
#define DISPLAY_SC_DESTROYED_STATE 3


/*****************************************************************************
 * Headers for type : Serial
 *****************************************************************************/

// Definition of the instance struct:
struct Serial_Instance {

// Instances of different sessions
bool active;
// Variables for the ID of the ports of the instance
uint16_t id_rx;
uint16_t id_tx;
// Variables for the current instance state
int Serial_SerialImpl_State;
// Variables for the properties of the instance

};
// Declaration of prototypes outgoing messages :
void Serial_SerialImpl_OnEntry(int state, struct Serial_Instance *_instance);
void Serial_handle_tx_write_byte(struct Serial_Instance *_instance, uint8_t b);
// Declaration of callbacks for incoming messages:
void register_Serial_send_rx_receive_byte_listener(void (*_listener)(struct Serial_Instance *, uint8_t));
void register_external_Serial_send_rx_receive_byte_listener(void (*_listener)(struct Serial_Instance *, uint8_t));

// Definition of the states:
#define SERIAL_SERIALIMPL_RECEIVING_STATE 0
#define SERIAL_SERIALIMPL_STATE 1



struct timer2_instance_type {
    uint16_t listener_id;
    /*INSTANCE_INFORMATION*/
};
extern struct timer2_instance_type timer2_instance;

void timer2_setup();
void timer2_read();

//void forward_timer2_SoftButton_send_Timer_timer_start(struct SoftButton_Instance *_instance, uint8_t id, uint32_t time);
//void forward_timer2_SoftButton_send_Timer_timer_cancel(struct SoftButton_Instance *_instance, uint8_t id);
void forward_timer2_Main_send_clock_timer_start(struct Main_Instance *_instance, uint8_t id, uint32_t time);
void forward_timer2_Main_send_clock_timer_cancel(struct Main_Instance *_instance, uint8_t id);
/*****************************************************************************
 * Headers for type : Main
 *****************************************************************************/

// Definition of the instance struct:
struct Main_Instance {

// Instances of different sessions
bool active;
// Variables for the ID of the ports of the instance
uint16_t id_clock;
uint16_t id_gateway;
uint16_t id_display;
// Variables for the current instance state
int Main_Main_State;
// Variables for the properties of the instance
int16_t Main_value_var;
uint8_t Main_XDISPSIZE_var;
uint8_t Main_YDISPSIZE_var;
uint8_t * Main_fgcolor_var;
uint16_t Main_fgcolor_var_size;
uint8_t * Main_bgcolor_var;
uint16_t Main_bgcolor_var_size;

};
// Declaration of prototypes outgoing messages :
void Main_Main_OnEntry(int state, struct Main_Instance *_instance);
void Main_handle_gateway_clear_screen(struct Main_Instance *_instance);
void Main_handle_gateway_display_digit(struct Main_Instance *_instance, int8_t d);
void Main_handle_clock_timer_timeout(struct Main_Instance *_instance, uint8_t id);
// Declaration of callbacks for incoming messages:
void register_Main_send_clock_timer_start_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint32_t));
void register_external_Main_send_clock_timer_start_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint32_t));
void register_Main_send_clock_timer_cancel_listener(void (*_listener)(struct Main_Instance *, uint8_t));
void register_external_Main_send_clock_timer_cancel_listener(void (*_listener)(struct Main_Instance *, uint8_t));
void register_Main_send_gateway_button_pushed_listener(void (*_listener)(struct Main_Instance *));
void register_external_Main_send_gateway_button_pushed_listener(void (*_listener)(struct Main_Instance *));
void register_Main_send_display_create_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t));
void register_external_Main_send_display_create_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t));
void register_Main_send_display_destroy_listener(void (*_listener)(struct Main_Instance *));
void register_external_Main_send_display_destroy_listener(void (*_listener)(struct Main_Instance *));
void register_Main_send_display_update_listener(void (*_listener)(struct Main_Instance *));
void register_external_Main_send_display_update_listener(void (*_listener)(struct Main_Instance *));
void register_Main_send_display_clear_listener(void (*_listener)(struct Main_Instance *));
void register_external_Main_send_display_clear_listener(void (*_listener)(struct Main_Instance *));
void register_Main_send_display_setColor_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t));
void register_external_Main_send_display_setColor_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t));
void register_Main_send_display_setBGColor_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t));
void register_external_Main_send_display_setBGColor_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t));
void register_Main_send_display_drawRect_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t, uint8_t));
void register_external_Main_send_display_drawRect_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t, uint8_t));
void register_Main_send_display_fillRect_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t, uint8_t));
void register_external_Main_send_display_fillRect_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t, uint8_t));
void register_Main_send_display_drawInteger_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, int16_t, uint8_t, uint8_t));
void register_external_Main_send_display_drawInteger_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, int16_t, uint8_t, uint8_t));
void register_Main_send_display_drawThingML_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t));
void register_external_Main_send_display_drawThingML_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t));

// Definition of the states:
#define MAIN_MAIN_STATE 0
#define MAIN_MAIN_START_STATE 1



/*****************************************************************************
 * Implementation for type : DisplayArduino
 *****************************************************************************/


// BEGIN: Code from the c_global annotation DisplayArduino
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);
// END: Code from the c_global annotation DisplayArduino

// Declaration of prototypes:
//Prototypes: State Machine
void Display_SC_OnExit(int state, struct DisplayArduino_Instance *_instance);
//Prototypes: Message Sending
void DisplayArduino_send_display_displayReady(struct DisplayArduino_Instance *_instance);
void DisplayArduino_send_display_displayError(struct DisplayArduino_Instance *_instance);
//Prototypes: Function
void f_DisplayArduino_drawDigit(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, uint8_t d, uint8_t size);
void f_DisplayArduino_drawThingML(struct DisplayArduino_Instance *_instance, int16_t px, int16_t py);
void f_DisplayArduino_drawInteger(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, int16_t v, uint8_t digits, uint8_t scale);
void f_DisplayArduino_clearInteger(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, uint8_t digits, uint8_t scale);
void f_DisplayArduino_initDisplay(struct DisplayArduino_Instance *_instance, uint8_t xsize, uint8_t ysize);
void f_DisplayArduino_destroyDisplay(struct DisplayArduino_Instance *_instance);
void f_DisplayArduino_clearScreen(struct DisplayArduino_Instance *_instance);
void f_DisplayArduino_setColor(struct DisplayArduino_Instance *_instance, uint8_t r, uint8_t g, uint8_t b);
void f_DisplayArduino_drawRect(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height);
void f_DisplayArduino_fillRect(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height);
void f_DisplayArduino_updateDisplay(struct DisplayArduino_Instance *_instance);
// Declaration of functions:
// Definition of function drawDigit
void f_DisplayArduino_drawDigit(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, uint8_t d, uint8_t size) {
if(d < 1) {
f_DisplayArduino_fillRect(_instance, x, y, size, 5 * size);
f_DisplayArduino_fillRect(_instance, x, y, 3 * size, size);
f_DisplayArduino_fillRect(_instance, x + 2 * size, y, size, 5 * size);
f_DisplayArduino_fillRect(_instance, x, y + 4 * size, 3 * size, size);

} else {
if(d < 2) {
f_DisplayArduino_fillRect(_instance, x + size, y, size, 5 * size);

} else {
if(d < 3) {
f_DisplayArduino_fillRect(_instance, x, y, size * 3, size);
f_DisplayArduino_fillRect(_instance, x + 2 * size, y, size, 3 * size);
f_DisplayArduino_fillRect(_instance, x, y + 2 * size, 3 * size, size);
f_DisplayArduino_fillRect(_instance, x, y + 2 * size, size, 3 * size);
f_DisplayArduino_fillRect(_instance, x, y + 4 * size, 3 * size, size);

} else {
if(d < 4) {
f_DisplayArduino_fillRect(_instance, x, y, size * 3, size);
f_DisplayArduino_fillRect(_instance, x + 2 * size, y, size, 5 * size);
f_DisplayArduino_fillRect(_instance, x, y + 4 * size, 3 * size, size);
f_DisplayArduino_fillRect(_instance, x + size, y + 2 * size, 2 * size, size);

} else {
if(d < 5) {
f_DisplayArduino_fillRect(_instance, x, y, size, 3 * size);
f_DisplayArduino_fillRect(_instance, x, y + 2 * size, 3 * size, size);
f_DisplayArduino_fillRect(_instance, x + 2 * size, y, size, 5 * size);

} else {
if(d < 6) {
f_DisplayArduino_fillRect(_instance, x, y, size * 3, size);
f_DisplayArduino_fillRect(_instance, x, y, size, 3 * size);
f_DisplayArduino_fillRect(_instance, x, y + 2 * size, 3 * size, size);
f_DisplayArduino_fillRect(_instance, x + 2 * size, y + 2 * size, size, 3 * size);
f_DisplayArduino_fillRect(_instance, x, y + 4 * size, 3 * size, size);

} else {
if(d < 7) {
f_DisplayArduino_fillRect(_instance, x, y, size * 3, size);
f_DisplayArduino_fillRect(_instance, x, y, size, 5 * size);
f_DisplayArduino_fillRect(_instance, x, y + 2 * size, 3 * size, size);
f_DisplayArduino_fillRect(_instance, x + 2 * size, y + 2 * size, size, 3 * size);
f_DisplayArduino_fillRect(_instance, x, y + 4 * size, 3 * size, size);

} else {
if(d < 8) {
f_DisplayArduino_fillRect(_instance, x, y, 3 * size, size);
f_DisplayArduino_fillRect(_instance, x + 2 * size, y, size, 5 * size);

} else {
if(d < 9) {
f_DisplayArduino_fillRect(_instance, x, y, size, 5 * size);
f_DisplayArduino_fillRect(_instance, x, y, 3 * size, size);
f_DisplayArduino_fillRect(_instance, x + 2 * size, y, size, 5 * size);
f_DisplayArduino_fillRect(_instance, x, y + 4 * size, 3 * size, size);
f_DisplayArduino_fillRect(_instance, x, y + 2 * size, 3 * size, size);

} else {
f_DisplayArduino_fillRect(_instance, x, y, size, 3 * size);
f_DisplayArduino_fillRect(_instance, x, y, 3 * size, size);
f_DisplayArduino_fillRect(_instance, x + 2 * size, y, size, 5 * size);
f_DisplayArduino_fillRect(_instance, x, y + 4 * size, 3 * size, size);
f_DisplayArduino_fillRect(_instance, x, y + 2 * size, 3 * size, size);

}

}

}

}

}

}

}

}

}
}
// Definition of function drawThingML
void f_DisplayArduino_drawThingML(struct DisplayArduino_Instance *_instance, int16_t px, int16_t py) {
f_DisplayArduino_setColor(_instance, 255, 255, 255);
f_DisplayArduino_fillRect(_instance, px, py, 108, 13);
int16_t x = px + 1;
int16_t y = py + 1;
f_DisplayArduino_setColor(_instance, 80, 80, 80);
f_DisplayArduino_fillRect(_instance, x + 0, y + 0, 12, 2);
f_DisplayArduino_fillRect(_instance, x + 5, y + 0, 2, 11);
f_DisplayArduino_fillRect(_instance, x + 18, y + 0, 2, 11);
f_DisplayArduino_fillRect(_instance, x + 27, y + 0, 2, 11);
f_DisplayArduino_fillRect(_instance, x + 18, y + 5, 11, 2);
f_DisplayArduino_fillRect(_instance, x + 36, y + 0, 2, 11);
f_DisplayArduino_fillRect(_instance, x + 44, y + 0, 2, 11);
f_DisplayArduino_fillRect(_instance, x + 46, y + 1, 1, 3);
f_DisplayArduino_fillRect(_instance, x + 47, y + 2, 1, 3);
f_DisplayArduino_fillRect(_instance, x + 48, y + 3, 1, 3);
f_DisplayArduino_fillRect(_instance, x + 49, y + 4, 1, 3);
f_DisplayArduino_fillRect(_instance, x + 50, y + 5, 1, 3);
f_DisplayArduino_fillRect(_instance, x + 51, y + 6, 1, 3);
f_DisplayArduino_fillRect(_instance, x + 52, y + 7, 1, 3);
f_DisplayArduino_fillRect(_instance, x + 53, y + 0, 2, 11);
f_DisplayArduino_fillRect(_instance, x + 62, y + 0, 2, 11);
f_DisplayArduino_fillRect(_instance, x + 62, y + 0, 12, 2);
f_DisplayArduino_fillRect(_instance, x + 62, y + 9, 12, 2);
f_DisplayArduino_fillRect(_instance, x + 62, y + 9, 12, 2);
f_DisplayArduino_fillRect(_instance, x + 69, y + 5, 5, 2);
f_DisplayArduino_fillRect(_instance, x + 72, y + 7, 2, 2);
f_DisplayArduino_setColor(_instance, 120, 232, 110);
f_DisplayArduino_fillRect(_instance, x + 80, y + 0, 11, 2);
f_DisplayArduino_fillRect(_instance, x + 80, y + 0, 2, 11);
f_DisplayArduino_fillRect(_instance, x + 85, y + 0, 2, 11);
f_DisplayArduino_fillRect(_instance, x + 89, y + 0, 2, 11);
f_DisplayArduino_fillRect(_instance, x + 95, y + 0, 2, 11);
f_DisplayArduino_fillRect(_instance, x + 95, y + 9, 11, 2);
}
// Definition of function drawInteger
void f_DisplayArduino_drawInteger(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, int16_t v, uint8_t digits, uint8_t scale) {
f_DisplayArduino_clearInteger(_instance, x, y, digits, scale);
int16_t val = v;
uint8_t d = digits;
while(d > 0) {
f_DisplayArduino_drawDigit(_instance, x + (d - 1) * 4 * scale, y, val % 10, scale);
val = val / 10;
d = d - 1;

}
}
// Definition of function clearInteger
void f_DisplayArduino_clearInteger(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, uint8_t digits, uint8_t scale) {
f_DisplayArduino_setColor(_instance, _instance->Display_bg_r_var, _instance->Display_bg_g_var, _instance->Display_bg_b_var);
f_DisplayArduino_fillRect(_instance, x, y, (digits * 4 - 1) * scale, 5 * scale);
f_DisplayArduino_setColor(_instance, _instance->Display_fg_r_var, _instance->Display_fg_g_var, _instance->Display_fg_b_var);
}
// Definition of function initDisplay
void f_DisplayArduino_initDisplay(struct DisplayArduino_Instance *_instance, uint8_t xsize, uint8_t ysize) {
tft.initR(INITR_BLACKTAB);
		tft.fillScreen(ST7735_BLACK);
		tft.setRotation(3);
}
// Definition of function destroyDisplay
void f_DisplayArduino_destroyDisplay(struct DisplayArduino_Instance *_instance) {
f_DisplayArduino_clearScreen(_instance);
}
// Definition of function clearScreen
void f_DisplayArduino_clearScreen(struct DisplayArduino_Instance *_instance) {
tft.fillScreen(ST7735_BLACK);
}
// Definition of function setColor
void f_DisplayArduino_setColor(struct DisplayArduino_Instance *_instance, uint8_t r, uint8_t g, uint8_t b) {
_instance->DisplayArduino_color_var = tft.color565(r, g, b);
}
// Definition of function drawRect
void f_DisplayArduino_drawRect(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height) {
tft.drawRect(x,y,width,height,_instance->DisplayArduino_color_var);
}
// Definition of function fillRect
void f_DisplayArduino_fillRect(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height) {
tft.fillRect(x,y,width,height,_instance->DisplayArduino_color_var);
}
// Definition of function updateDisplay
void f_DisplayArduino_updateDisplay(struct DisplayArduino_Instance *_instance) {
}

// Sessions functionss:


// On Entry Actions:
void Display_SC_OnEntry(int state, struct DisplayArduino_Instance *_instance) {
switch(state) {
case DISPLAY_SC_STATE:{
_instance->Display_SC_State = DISPLAY_SC_WAIT_STATE;
Display_SC_OnEntry(_instance->Display_SC_State, _instance);
break;
}
case DISPLAY_SC_WAIT_STATE:{
break;
}
case DISPLAY_SC_RUNNING_STATE:{
DisplayArduino_send_display_displayReady(_instance);
break;
}
case DISPLAY_SC_DESTROYED_STATE:{
break;
}
default: break;
}
}

// On Exit Actions:
void Display_SC_OnExit(int state, struct DisplayArduino_Instance *_instance) {
switch(state) {
case DISPLAY_SC_STATE:{
Display_SC_OnExit(_instance->Display_SC_State, _instance);
break;}
case DISPLAY_SC_WAIT_STATE:{
break;}
case DISPLAY_SC_RUNNING_STATE:{
break;}
case DISPLAY_SC_DESTROYED_STATE:{
break;}
default: break;
}
}

// Event Handlers for incoming messages:
void DisplayArduino_handle_display_drawInteger(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, int16_t v, uint8_t digits, uint8_t scale) {
if(!(_instance->active)) return;
//Region SC
uint8_t Display_SC_State_event_consumed = 0;
if (_instance->Display_SC_State == DISPLAY_SC_RUNNING_STATE) {
if (Display_SC_State_event_consumed == 0 && 1) {
f_DisplayArduino_drawInteger(_instance, x, y, v, digits, scale);
Display_SC_State_event_consumed = 1;
}
}
//End Region SC
//End dsregion SC
//Session list: 
}
void DisplayArduino_handle_display_create(struct DisplayArduino_Instance *_instance, uint8_t xsize, uint8_t ysize) {
if(!(_instance->active)) return;
//Region SC
uint8_t Display_SC_State_event_consumed = 0;
if (_instance->Display_SC_State == DISPLAY_SC_WAIT_STATE) {
if (Display_SC_State_event_consumed == 0 && 1) {
Display_SC_OnExit(DISPLAY_SC_WAIT_STATE, _instance);
_instance->Display_SC_State = DISPLAY_SC_RUNNING_STATE;
f_DisplayArduino_initDisplay(_instance, xsize, ysize);
Display_SC_OnEntry(DISPLAY_SC_RUNNING_STATE, _instance);
Display_SC_State_event_consumed = 1;
}
}
//End Region SC
//End dsregion SC
//Session list: 
}
void DisplayArduino_handle_display_fillRect(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height) {
if(!(_instance->active)) return;
//Region SC
uint8_t Display_SC_State_event_consumed = 0;
if (_instance->Display_SC_State == DISPLAY_SC_RUNNING_STATE) {
if (Display_SC_State_event_consumed == 0 && 1) {
f_DisplayArduino_fillRect(_instance, x, y, width, height);
Display_SC_State_event_consumed = 1;
}
}
//End Region SC
//End dsregion SC
//Session list: 
}
void DisplayArduino_handle_display_update(struct DisplayArduino_Instance *_instance) {
if(!(_instance->active)) return;
//Region SC
uint8_t Display_SC_State_event_consumed = 0;
if (_instance->Display_SC_State == DISPLAY_SC_RUNNING_STATE) {
if (Display_SC_State_event_consumed == 0 && 1) {
f_DisplayArduino_updateDisplay(_instance);
Display_SC_State_event_consumed = 1;
}
}
//End Region SC
//End dsregion SC
//Session list: 
}
void DisplayArduino_handle_display_setBGColor(struct DisplayArduino_Instance *_instance, uint8_t r, uint8_t g, uint8_t b) {
if(!(_instance->active)) return;
//Region SC
uint8_t Display_SC_State_event_consumed = 0;
if (_instance->Display_SC_State == DISPLAY_SC_RUNNING_STATE) {
if (Display_SC_State_event_consumed == 0 && 1) {
_instance->Display_bg_r_var = r;
_instance->Display_bg_g_var = g;
_instance->Display_bg_b_var = b;
Display_SC_State_event_consumed = 1;
}
}
//End Region SC
//End dsregion SC
//Session list: 
}
void DisplayArduino_handle_display_drawThingML(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y) {
if(!(_instance->active)) return;
//Region SC
uint8_t Display_SC_State_event_consumed = 0;
if (_instance->Display_SC_State == DISPLAY_SC_RUNNING_STATE) {
if (Display_SC_State_event_consumed == 0 && 1) {
f_DisplayArduino_drawThingML(_instance, x, y);
Display_SC_State_event_consumed = 1;
}
}
//End Region SC
//End dsregion SC
//Session list: 
}
void DisplayArduino_handle_display_setColor(struct DisplayArduino_Instance *_instance, uint8_t r, uint8_t g, uint8_t b) {
if(!(_instance->active)) return;
//Region SC
uint8_t Display_SC_State_event_consumed = 0;
if (_instance->Display_SC_State == DISPLAY_SC_RUNNING_STATE) {
if (Display_SC_State_event_consumed == 0 && 1) {
_instance->Display_fg_r_var = r;
_instance->Display_fg_g_var = g;
_instance->Display_fg_b_var = b;
f_DisplayArduino_setColor(_instance, r, g, b);
Display_SC_State_event_consumed = 1;
}
}
//End Region SC
//End dsregion SC
//Session list: 
}
void DisplayArduino_handle_display_destroy(struct DisplayArduino_Instance *_instance) {
if(!(_instance->active)) return;
//Region SC
uint8_t Display_SC_State_event_consumed = 0;
if (_instance->Display_SC_State == DISPLAY_SC_RUNNING_STATE) {
if (Display_SC_State_event_consumed == 0 && 1) {
Display_SC_OnExit(DISPLAY_SC_RUNNING_STATE, _instance);
_instance->Display_SC_State = DISPLAY_SC_DESTROYED_STATE;
f_DisplayArduino_destroyDisplay(_instance);
Display_SC_OnEntry(DISPLAY_SC_DESTROYED_STATE, _instance);
Display_SC_State_event_consumed = 1;
}
}
//End Region SC
//End dsregion SC
//Session list: 
}
void DisplayArduino_handle_display_clear(struct DisplayArduino_Instance *_instance) {
if(!(_instance->active)) return;
//Region SC
uint8_t Display_SC_State_event_consumed = 0;
if (_instance->Display_SC_State == DISPLAY_SC_RUNNING_STATE) {
if (Display_SC_State_event_consumed == 0 && 1) {
f_DisplayArduino_clearScreen(_instance);
Display_SC_State_event_consumed = 1;
}
}
//End Region SC
//End dsregion SC
//Session list: 
}
void DisplayArduino_handle_display_drawRect(struct DisplayArduino_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height) {
if(!(_instance->active)) return;
//Region SC
uint8_t Display_SC_State_event_consumed = 0;
if (_instance->Display_SC_State == DISPLAY_SC_RUNNING_STATE) {
if (Display_SC_State_event_consumed == 0 && 1) {
f_DisplayArduino_drawRect(_instance, x, y, width, height);
Display_SC_State_event_consumed = 1;
}
}
//End Region SC
//End dsregion SC
//Session list: 
}

// Observers for outgoing messages:
void (*external_DisplayArduino_send_display_displayReady_listener)(struct DisplayArduino_Instance *)= 0x0;
void (*DisplayArduino_send_display_displayReady_listener)(struct DisplayArduino_Instance *)= 0x0;
void register_external_DisplayArduino_send_display_displayReady_listener(void (*_listener)(struct DisplayArduino_Instance *)){
external_DisplayArduino_send_display_displayReady_listener = _listener;
}
void register_DisplayArduino_send_display_displayReady_listener(void (*_listener)(struct DisplayArduino_Instance *)){
DisplayArduino_send_display_displayReady_listener = _listener;
}
void DisplayArduino_send_display_displayReady(struct DisplayArduino_Instance *_instance){
if (DisplayArduino_send_display_displayReady_listener != 0x0) DisplayArduino_send_display_displayReady_listener(_instance);
if (external_DisplayArduino_send_display_displayReady_listener != 0x0) external_DisplayArduino_send_display_displayReady_listener(_instance);
;
}
void (*external_DisplayArduino_send_display_displayError_listener)(struct DisplayArduino_Instance *)= 0x0;
void (*DisplayArduino_send_display_displayError_listener)(struct DisplayArduino_Instance *)= 0x0;
void register_external_DisplayArduino_send_display_displayError_listener(void (*_listener)(struct DisplayArduino_Instance *)){
external_DisplayArduino_send_display_displayError_listener = _listener;
}
void register_DisplayArduino_send_display_displayError_listener(void (*_listener)(struct DisplayArduino_Instance *)){
DisplayArduino_send_display_displayError_listener = _listener;
}
void DisplayArduino_send_display_displayError(struct DisplayArduino_Instance *_instance){
if (DisplayArduino_send_display_displayError_listener != 0x0) DisplayArduino_send_display_displayError_listener(_instance);
if (external_DisplayArduino_send_display_displayError_listener != 0x0) external_DisplayArduino_send_display_displayError_listener(_instance);
;
}



/*****************************************************************************
 * Implementation for type : Main
 *****************************************************************************/

// Declaration of prototypes:
//Prototypes: State Machine
void Main_Main_OnExit(int state, struct Main_Instance *_instance);
//Prototypes: Message Sending
void Main_send_clock_timer_start(struct Main_Instance *_instance, uint8_t id, uint32_t time);
void Main_send_clock_timer_cancel(struct Main_Instance *_instance, uint8_t id);
void Main_send_gateway_button_pushed(struct Main_Instance *_instance);
void Main_send_display_create(struct Main_Instance *_instance, uint8_t xsize, uint8_t ysize);
void Main_send_display_destroy(struct Main_Instance *_instance);
void Main_send_display_update(struct Main_Instance *_instance);
void Main_send_display_clear(struct Main_Instance *_instance);
void Main_send_display_setColor(struct Main_Instance *_instance, uint8_t r, uint8_t g, uint8_t b);
void Main_send_display_setBGColor(struct Main_Instance *_instance, uint8_t r, uint8_t g, uint8_t b);
void Main_send_display_drawRect(struct Main_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height);
void Main_send_display_fillRect(struct Main_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height);
void Main_send_display_drawInteger(struct Main_Instance *_instance, uint8_t x, uint8_t y, int16_t v, uint8_t digits, uint8_t scale);
void Main_send_display_drawThingML(struct Main_Instance *_instance, uint8_t x, uint8_t y);
//Prototypes: Function
void f_Main_initColors(struct Main_Instance *_instance);
void f_Main_clearScreen(struct Main_Instance *_instance);
void f_Main_drawValue(struct Main_Instance *_instance);
// Declaration of functions:
// Definition of function initColors
void f_Main_initColors(struct Main_Instance *_instance) {
_instance->Main_bgcolor_var[0] = 53;
_instance->Main_bgcolor_var[1] = 40;
_instance->Main_bgcolor_var[2] = 120;
_instance->Main_fgcolor_var[0] = 227;
_instance->Main_fgcolor_var[1] = 94;
_instance->Main_fgcolor_var[2] = 174;
Main_send_display_setBGColor(_instance, _instance->Main_bgcolor_var[0]
, _instance->Main_bgcolor_var[1]
, _instance->Main_bgcolor_var[2]
);
Main_send_display_setColor(_instance, _instance->Main_fgcolor_var[0]
, _instance->Main_fgcolor_var[1]
, _instance->Main_fgcolor_var[2]
);
}
// Definition of function clearScreen
void f_Main_clearScreen(struct Main_Instance *_instance) {
f_Main_initColors(_instance);
Main_send_display_setColor(_instance, 255, 255, 255);
Main_send_display_fillRect(_instance, 8, 30, 142, 76);
Main_send_display_setColor(_instance, _instance->Main_fgcolor_var[0]
, _instance->Main_fgcolor_var[1]
, _instance->Main_fgcolor_var[2]
);
Main_send_display_fillRect(_instance, 9, 31, 140, 50);
Main_send_display_setBGColor(_instance, _instance->Main_fgcolor_var[0]
, _instance->Main_fgcolor_var[1]
, _instance->Main_fgcolor_var[2]
);
Main_send_display_drawThingML(_instance, 26, 87);
Main_send_display_update(_instance);
}
// Definition of function drawValue
void f_Main_drawValue(struct Main_Instance *_instance) {
Main_send_display_setColor(_instance, 200, 255, 170);
Main_send_display_drawInteger(_instance, 23, 40, _instance->Main_value_var, 5, 6);
Main_send_display_update(_instance);
}

// Sessions functionss:


// On Entry Actions:
void Main_Main_OnEntry(int state, struct Main_Instance *_instance) {
switch(state) {
case MAIN_MAIN_STATE:{
_instance->Main_Main_State = MAIN_MAIN_START_STATE;
Main_send_display_create(_instance, _instance->Main_XDISPSIZE_var, _instance->Main_YDISPSIZE_var);
Main_send_display_clear(_instance);
f_Main_initColors(_instance);
Main_send_clock_timer_start(_instance, 1, 50);
Main_Main_OnEntry(_instance->Main_Main_State, _instance);
break;
}
case MAIN_MAIN_START_STATE:{
f_Main_clearScreen(_instance);
break;
}
default: break;
}
}

// On Exit Actions:
void Main_Main_OnExit(int state, struct Main_Instance *_instance) {
switch(state) {
case MAIN_MAIN_STATE:{
Main_Main_OnExit(_instance->Main_Main_State, _instance);
break;}
case MAIN_MAIN_START_STATE:{
break;}
default: break;
}
}

// Event Handlers for incoming messages:
void Main_handle_gateway_clear_screen(struct Main_Instance *_instance) {
if(!(_instance->active)) return;
//Region Main
uint8_t Main_Main_State_event_consumed = 0;
if (_instance->Main_Main_State == MAIN_MAIN_START_STATE) {
if (Main_Main_State_event_consumed == 0 && 1) {
Main_send_clock_timer_start(_instance, 0, 100);
_instance->Main_value_var = 0;
Main_Main_State_event_consumed = 1;
}
}
//End Region Main
//End dsregion Main
//Session list: 
}
void Main_handle_gateway_display_digit(struct Main_Instance *_instance, int8_t d) {
if(!(_instance->active)) return;
//Region Main
uint8_t Main_Main_State_event_consumed = 0;
if (_instance->Main_Main_State == MAIN_MAIN_START_STATE) {
if (Main_Main_State_event_consumed == 0 && 1) {
_instance->Main_value_var = _instance->Main_value_var * 10 + d;
Main_send_clock_timer_start(_instance, 0, 100);
Main_Main_State_event_consumed = 1;
}
}
//End Region Main
//End dsregion Main
//Session list: 
}
void Main_handle_clock_timer_timeout(struct Main_Instance *_instance, uint8_t id) {
if(!(_instance->active)) return;
//Region Main
uint8_t Main_Main_State_event_consumed = 0;
if (_instance->Main_Main_State == MAIN_MAIN_START_STATE) {
if (Main_Main_State_event_consumed == 0 && id == 0) {
f_Main_drawValue(_instance);
Main_Main_State_event_consumed = 1;
}
}
//End Region Main
//End dsregion Main
//Session list: 
if (id == 1) {
int16_t a = analogRead(3);
if(a > 205 && a < 300) {
Main_send_display_setColor(_instance, 2, 50, 250);
Main_send_display_drawRect(_instance, 0, 0, _instance->Main_XDISPSIZE_var - 1, _instance->Main_YDISPSIZE_var - 1);
Main_send_display_drawRect(_instance, 1, 1, _instance->Main_XDISPSIZE_var - 3, _instance->Main_YDISPSIZE_var - 3);
Main_send_clock_timer_start(_instance, 1, 500);
Main_send_gateway_button_pushed(_instance);

} else {
Main_send_display_setColor(_instance, 0, 0, 0);
Main_send_display_drawRect(_instance, 0, 0, _instance->Main_XDISPSIZE_var - 1, _instance->Main_YDISPSIZE_var - 1);
Main_send_display_drawRect(_instance, 1, 1, _instance->Main_XDISPSIZE_var - 3, _instance->Main_YDISPSIZE_var - 3);
Main_send_clock_timer_start(_instance, 1, 50);

}
Main_Main_State_event_consumed = 1;
}
}

// Observers for outgoing messages:
void (*external_Main_send_clock_timer_start_listener)(struct Main_Instance *, uint8_t, uint32_t)= 0x0;
void (*Main_send_clock_timer_start_listener)(struct Main_Instance *, uint8_t, uint32_t)= 0x0;
void register_external_Main_send_clock_timer_start_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint32_t)){
external_Main_send_clock_timer_start_listener = _listener;
}
void register_Main_send_clock_timer_start_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint32_t)){
Main_send_clock_timer_start_listener = _listener;
}
void Main_send_clock_timer_start(struct Main_Instance *_instance, uint8_t id, uint32_t time){
if (Main_send_clock_timer_start_listener != 0x0) Main_send_clock_timer_start_listener(_instance, id, time);
if (external_Main_send_clock_timer_start_listener != 0x0) external_Main_send_clock_timer_start_listener(_instance, id, time);
;
}
void (*external_Main_send_clock_timer_cancel_listener)(struct Main_Instance *, uint8_t)= 0x0;
void (*Main_send_clock_timer_cancel_listener)(struct Main_Instance *, uint8_t)= 0x0;
void register_external_Main_send_clock_timer_cancel_listener(void (*_listener)(struct Main_Instance *, uint8_t)){
external_Main_send_clock_timer_cancel_listener = _listener;
}
void register_Main_send_clock_timer_cancel_listener(void (*_listener)(struct Main_Instance *, uint8_t)){
Main_send_clock_timer_cancel_listener = _listener;
}
void Main_send_clock_timer_cancel(struct Main_Instance *_instance, uint8_t id){
if (Main_send_clock_timer_cancel_listener != 0x0) Main_send_clock_timer_cancel_listener(_instance, id);
if (external_Main_send_clock_timer_cancel_listener != 0x0) external_Main_send_clock_timer_cancel_listener(_instance, id);
;
}
void (*external_Main_send_gateway_button_pushed_listener)(struct Main_Instance *)= 0x0;
void (*Main_send_gateway_button_pushed_listener)(struct Main_Instance *)= 0x0;
void register_external_Main_send_gateway_button_pushed_listener(void (*_listener)(struct Main_Instance *)){
external_Main_send_gateway_button_pushed_listener = _listener;
}
void register_Main_send_gateway_button_pushed_listener(void (*_listener)(struct Main_Instance *)){
Main_send_gateway_button_pushed_listener = _listener;
}
void Main_send_gateway_button_pushed(struct Main_Instance *_instance){
if (Main_send_gateway_button_pushed_listener != 0x0) Main_send_gateway_button_pushed_listener(_instance);
if (external_Main_send_gateway_button_pushed_listener != 0x0) external_Main_send_gateway_button_pushed_listener(_instance);
;
}
void (*external_Main_send_display_create_listener)(struct Main_Instance *, uint8_t, uint8_t)= 0x0;
void (*Main_send_display_create_listener)(struct Main_Instance *, uint8_t, uint8_t)= 0x0;
void register_external_Main_send_display_create_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t)){
external_Main_send_display_create_listener = _listener;
}
void register_Main_send_display_create_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t)){
Main_send_display_create_listener = _listener;
}
void Main_send_display_create(struct Main_Instance *_instance, uint8_t xsize, uint8_t ysize){
if (Main_send_display_create_listener != 0x0) Main_send_display_create_listener(_instance, xsize, ysize);
if (external_Main_send_display_create_listener != 0x0) external_Main_send_display_create_listener(_instance, xsize, ysize);
;
}
void (*external_Main_send_display_destroy_listener)(struct Main_Instance *)= 0x0;
void (*Main_send_display_destroy_listener)(struct Main_Instance *)= 0x0;
void register_external_Main_send_display_destroy_listener(void (*_listener)(struct Main_Instance *)){
external_Main_send_display_destroy_listener = _listener;
}
void register_Main_send_display_destroy_listener(void (*_listener)(struct Main_Instance *)){
Main_send_display_destroy_listener = _listener;
}
void Main_send_display_destroy(struct Main_Instance *_instance){
if (Main_send_display_destroy_listener != 0x0) Main_send_display_destroy_listener(_instance);
if (external_Main_send_display_destroy_listener != 0x0) external_Main_send_display_destroy_listener(_instance);
;
}
void (*external_Main_send_display_update_listener)(struct Main_Instance *)= 0x0;
void (*Main_send_display_update_listener)(struct Main_Instance *)= 0x0;
void register_external_Main_send_display_update_listener(void (*_listener)(struct Main_Instance *)){
external_Main_send_display_update_listener = _listener;
}
void register_Main_send_display_update_listener(void (*_listener)(struct Main_Instance *)){
Main_send_display_update_listener = _listener;
}
void Main_send_display_update(struct Main_Instance *_instance){
if (Main_send_display_update_listener != 0x0) Main_send_display_update_listener(_instance);
if (external_Main_send_display_update_listener != 0x0) external_Main_send_display_update_listener(_instance);
;
}
void (*external_Main_send_display_clear_listener)(struct Main_Instance *)= 0x0;
void (*Main_send_display_clear_listener)(struct Main_Instance *)= 0x0;
void register_external_Main_send_display_clear_listener(void (*_listener)(struct Main_Instance *)){
external_Main_send_display_clear_listener = _listener;
}
void register_Main_send_display_clear_listener(void (*_listener)(struct Main_Instance *)){
Main_send_display_clear_listener = _listener;
}
void Main_send_display_clear(struct Main_Instance *_instance){
if (Main_send_display_clear_listener != 0x0) Main_send_display_clear_listener(_instance);
if (external_Main_send_display_clear_listener != 0x0) external_Main_send_display_clear_listener(_instance);
;
}
void (*external_Main_send_display_setColor_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t)= 0x0;
void (*Main_send_display_setColor_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t)= 0x0;
void register_external_Main_send_display_setColor_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t)){
external_Main_send_display_setColor_listener = _listener;
}
void register_Main_send_display_setColor_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t)){
Main_send_display_setColor_listener = _listener;
}
void Main_send_display_setColor(struct Main_Instance *_instance, uint8_t r, uint8_t g, uint8_t b){
if (Main_send_display_setColor_listener != 0x0) Main_send_display_setColor_listener(_instance, r, g, b);
if (external_Main_send_display_setColor_listener != 0x0) external_Main_send_display_setColor_listener(_instance, r, g, b);
;
}
void (*external_Main_send_display_setBGColor_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t)= 0x0;
void (*Main_send_display_setBGColor_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t)= 0x0;
void register_external_Main_send_display_setBGColor_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t)){
external_Main_send_display_setBGColor_listener = _listener;
}
void register_Main_send_display_setBGColor_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t)){
Main_send_display_setBGColor_listener = _listener;
}
void Main_send_display_setBGColor(struct Main_Instance *_instance, uint8_t r, uint8_t g, uint8_t b){
if (Main_send_display_setBGColor_listener != 0x0) Main_send_display_setBGColor_listener(_instance, r, g, b);
if (external_Main_send_display_setBGColor_listener != 0x0) external_Main_send_display_setBGColor_listener(_instance, r, g, b);
;
}
void (*external_Main_send_display_drawRect_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t, uint8_t)= 0x0;
void (*Main_send_display_drawRect_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t, uint8_t)= 0x0;
void register_external_Main_send_display_drawRect_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t, uint8_t)){
external_Main_send_display_drawRect_listener = _listener;
}
void register_Main_send_display_drawRect_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t, uint8_t)){
Main_send_display_drawRect_listener = _listener;
}
void Main_send_display_drawRect(struct Main_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height){
if (Main_send_display_drawRect_listener != 0x0) Main_send_display_drawRect_listener(_instance, x, y, width, height);
if (external_Main_send_display_drawRect_listener != 0x0) external_Main_send_display_drawRect_listener(_instance, x, y, width, height);
;
}
void (*external_Main_send_display_fillRect_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t, uint8_t)= 0x0;
void (*Main_send_display_fillRect_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t, uint8_t)= 0x0;
void register_external_Main_send_display_fillRect_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t, uint8_t)){
external_Main_send_display_fillRect_listener = _listener;
}
void register_Main_send_display_fillRect_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, uint8_t, uint8_t)){
Main_send_display_fillRect_listener = _listener;
}
void Main_send_display_fillRect(struct Main_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height){
if (Main_send_display_fillRect_listener != 0x0) Main_send_display_fillRect_listener(_instance, x, y, width, height);
if (external_Main_send_display_fillRect_listener != 0x0) external_Main_send_display_fillRect_listener(_instance, x, y, width, height);
;
}
void (*external_Main_send_display_drawInteger_listener)(struct Main_Instance *, uint8_t, uint8_t, int16_t, uint8_t, uint8_t)= 0x0;
void (*Main_send_display_drawInteger_listener)(struct Main_Instance *, uint8_t, uint8_t, int16_t, uint8_t, uint8_t)= 0x0;
void register_external_Main_send_display_drawInteger_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, int16_t, uint8_t, uint8_t)){
external_Main_send_display_drawInteger_listener = _listener;
}
void register_Main_send_display_drawInteger_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t, int16_t, uint8_t, uint8_t)){
Main_send_display_drawInteger_listener = _listener;
}
void Main_send_display_drawInteger(struct Main_Instance *_instance, uint8_t x, uint8_t y, int16_t v, uint8_t digits, uint8_t scale){
if (Main_send_display_drawInteger_listener != 0x0) Main_send_display_drawInteger_listener(_instance, x, y, v, digits, scale);
if (external_Main_send_display_drawInteger_listener != 0x0) external_Main_send_display_drawInteger_listener(_instance, x, y, v, digits, scale);
;
}
void (*external_Main_send_display_drawThingML_listener)(struct Main_Instance *, uint8_t, uint8_t)= 0x0;
void (*Main_send_display_drawThingML_listener)(struct Main_Instance *, uint8_t, uint8_t)= 0x0;
void register_external_Main_send_display_drawThingML_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t)){
external_Main_send_display_drawThingML_listener = _listener;
}
void register_Main_send_display_drawThingML_listener(void (*_listener)(struct Main_Instance *, uint8_t, uint8_t)){
Main_send_display_drawThingML_listener = _listener;
}
void Main_send_display_drawThingML(struct Main_Instance *_instance, uint8_t x, uint8_t y){
if (Main_send_display_drawThingML_listener != 0x0) Main_send_display_drawThingML_listener(_instance, x, y);
if (external_Main_send_display_drawThingML_listener != 0x0) external_Main_send_display_drawThingML_listener(_instance, x, y);
;
}



/*****************************************************************************
 * Implementation for type : GatewayCommandParser
 *****************************************************************************/

// Declaration of prototypes:
//Prototypes: State Machine
void GatewayCommandParser_GatewayCommandParserSC_OnExit(int state, struct GatewayCommandParser_Instance *_instance);
//Prototypes: Message Sending
void GatewayCommandParser_send_bletx_write_byte(struct GatewayCommandParser_Instance *_instance, uint8_t b);
void GatewayCommandParser_send_bletx_print_message(struct GatewayCommandParser_Instance *_instance, char * msg);
void GatewayCommandParser_send_gateway_clear_screen(struct GatewayCommandParser_Instance *_instance);
void GatewayCommandParser_send_gateway_display_digit(struct GatewayCommandParser_Instance *_instance, int8_t d);
//Prototypes: Function
// Declaration of functions:

// Sessions functionss:


// On Entry Actions:
void GatewayCommandParser_GatewayCommandParserSC_OnEntry(int state, struct GatewayCommandParser_Instance *_instance) {
switch(state) {
case GATEWAYCOMMANDPARSER_GATEWAYCOMMANDPARSERSC_STATE:{
_instance->GatewayCommandParser_GatewayCommandParserSC_State = GATEWAYCOMMANDPARSER_GATEWAYCOMMANDPARSERSC_READY_STATE;
GatewayCommandParser_GatewayCommandParserSC_OnEntry(_instance->GatewayCommandParser_GatewayCommandParserSC_State, _instance);
break;
}
case GATEWAYCOMMANDPARSER_GATEWAYCOMMANDPARSERSC_READY_STATE:{
break;
}
default: break;
}
}

// On Exit Actions:
void GatewayCommandParser_GatewayCommandParserSC_OnExit(int state, struct GatewayCommandParser_Instance *_instance) {
switch(state) {
case GATEWAYCOMMANDPARSER_GATEWAYCOMMANDPARSERSC_STATE:{
GatewayCommandParser_GatewayCommandParserSC_OnExit(_instance->GatewayCommandParser_GatewayCommandParserSC_State, _instance);
break;}
case GATEWAYCOMMANDPARSER_GATEWAYCOMMANDPARSERSC_READY_STATE:{
break;}
default: break;
}
}

// Event Handlers for incoming messages:
void GatewayCommandParser_handle_blerx_receive_byte(struct GatewayCommandParser_Instance *_instance, uint8_t b) {
if(!(_instance->active)) return;
//Region GatewayCommandParserSC
uint8_t GatewayCommandParser_GatewayCommandParserSC_State_event_consumed = 0;
if (_instance->GatewayCommandParser_GatewayCommandParserSC_State == GATEWAYCOMMANDPARSER_GATEWAYCOMMANDPARSERSC_READY_STATE) {
if (GatewayCommandParser_GatewayCommandParserSC_State_event_consumed == 0 && b == '!') {
GatewayCommandParser_send_gateway_clear_screen(_instance);
GatewayCommandParser_GatewayCommandParserSC_State_event_consumed = 1;
}
else if (GatewayCommandParser_GatewayCommandParserSC_State_event_consumed == 0 && b > 47 && b < 58) {
GatewayCommandParser_send_gateway_display_digit(_instance, (b - 48));
GatewayCommandParser_GatewayCommandParserSC_State_event_consumed = 1;
}
}
//End Region GatewayCommandParserSC
//End dsregion GatewayCommandParserSC
//Session list: 
}
void GatewayCommandParser_handle_gateway_button_pushed(struct GatewayCommandParser_Instance *_instance) {
if(!(_instance->active)) return;
//Region GatewayCommandParserSC
uint8_t GatewayCommandParser_GatewayCommandParserSC_State_event_consumed = 0;
//End Region GatewayCommandParserSC
//End dsregion GatewayCommandParserSC
//Session list: 
if (1) {
GatewayCommandParser_send_bletx_write_byte(_instance, '?');
GatewayCommandParser_GatewayCommandParserSC_State_event_consumed = 1;
}
}

// Observers for outgoing messages:
void (*external_GatewayCommandParser_send_bletx_write_byte_listener)(struct GatewayCommandParser_Instance *, uint8_t)= 0x0;
void (*GatewayCommandParser_send_bletx_write_byte_listener)(struct GatewayCommandParser_Instance *, uint8_t)= 0x0;
void register_external_GatewayCommandParser_send_bletx_write_byte_listener(void (*_listener)(struct GatewayCommandParser_Instance *, uint8_t)){
external_GatewayCommandParser_send_bletx_write_byte_listener = _listener;
}
void register_GatewayCommandParser_send_bletx_write_byte_listener(void (*_listener)(struct GatewayCommandParser_Instance *, uint8_t)){
GatewayCommandParser_send_bletx_write_byte_listener = _listener;
}
void GatewayCommandParser_send_bletx_write_byte(struct GatewayCommandParser_Instance *_instance, uint8_t b){
if (GatewayCommandParser_send_bletx_write_byte_listener != 0x0) GatewayCommandParser_send_bletx_write_byte_listener(_instance, b);
if (external_GatewayCommandParser_send_bletx_write_byte_listener != 0x0) external_GatewayCommandParser_send_bletx_write_byte_listener(_instance, b);
;
}
void (*external_GatewayCommandParser_send_bletx_print_message_listener)(struct GatewayCommandParser_Instance *, char *)= 0x0;
void (*GatewayCommandParser_send_bletx_print_message_listener)(struct GatewayCommandParser_Instance *, char *)= 0x0;
void register_external_GatewayCommandParser_send_bletx_print_message_listener(void (*_listener)(struct GatewayCommandParser_Instance *, char *)){
external_GatewayCommandParser_send_bletx_print_message_listener = _listener;
}
void register_GatewayCommandParser_send_bletx_print_message_listener(void (*_listener)(struct GatewayCommandParser_Instance *, char *)){
GatewayCommandParser_send_bletx_print_message_listener = _listener;
}
void GatewayCommandParser_send_bletx_print_message(struct GatewayCommandParser_Instance *_instance, char * msg){
if (GatewayCommandParser_send_bletx_print_message_listener != 0x0) GatewayCommandParser_send_bletx_print_message_listener(_instance, msg);
if (external_GatewayCommandParser_send_bletx_print_message_listener != 0x0) external_GatewayCommandParser_send_bletx_print_message_listener(_instance, msg);
;
}
void (*external_GatewayCommandParser_send_gateway_clear_screen_listener)(struct GatewayCommandParser_Instance *)= 0x0;
void (*GatewayCommandParser_send_gateway_clear_screen_listener)(struct GatewayCommandParser_Instance *)= 0x0;
void register_external_GatewayCommandParser_send_gateway_clear_screen_listener(void (*_listener)(struct GatewayCommandParser_Instance *)){
external_GatewayCommandParser_send_gateway_clear_screen_listener = _listener;
}
void register_GatewayCommandParser_send_gateway_clear_screen_listener(void (*_listener)(struct GatewayCommandParser_Instance *)){
GatewayCommandParser_send_gateway_clear_screen_listener = _listener;
}
void GatewayCommandParser_send_gateway_clear_screen(struct GatewayCommandParser_Instance *_instance){
if (GatewayCommandParser_send_gateway_clear_screen_listener != 0x0) GatewayCommandParser_send_gateway_clear_screen_listener(_instance);
if (external_GatewayCommandParser_send_gateway_clear_screen_listener != 0x0) external_GatewayCommandParser_send_gateway_clear_screen_listener(_instance);
;
}
void (*external_GatewayCommandParser_send_gateway_display_digit_listener)(struct GatewayCommandParser_Instance *, int8_t)= 0x0;
void (*GatewayCommandParser_send_gateway_display_digit_listener)(struct GatewayCommandParser_Instance *, int8_t)= 0x0;
void register_external_GatewayCommandParser_send_gateway_display_digit_listener(void (*_listener)(struct GatewayCommandParser_Instance *, int8_t)){
external_GatewayCommandParser_send_gateway_display_digit_listener = _listener;
}
void register_GatewayCommandParser_send_gateway_display_digit_listener(void (*_listener)(struct GatewayCommandParser_Instance *, int8_t)){
GatewayCommandParser_send_gateway_display_digit_listener = _listener;
}
void GatewayCommandParser_send_gateway_display_digit(struct GatewayCommandParser_Instance *_instance, int8_t d){
if (GatewayCommandParser_send_gateway_display_digit_listener != 0x0) GatewayCommandParser_send_gateway_display_digit_listener(_instance, d);
if (external_GatewayCommandParser_send_gateway_display_digit_listener != 0x0) external_GatewayCommandParser_send_gateway_display_digit_listener(_instance, d);
;
}




#define MAX_INSTANCES 10
#define FIFO_SIZE 256

/*********************************
 * Instance IDs and lookup
 *********************************/

void * instances[MAX_INSTANCES];
uint16_t instances_count = 0;

void * instance_by_id(uint16_t id) {
  return instances[id];
}

uint16_t add_instance(void * instance_struct) {
  instances[instances_count] = instance_struct;
  return instances_count++;
}

/******************************************
 * Simple byte FIFO implementation
 ******************************************/

byte fifo[FIFO_SIZE];
int fifo_head = 0;
int fifo_tail = 0;

// Returns the number of byte currently in the fifo
int fifo_byte_length() {
  if (fifo_tail >= fifo_head)
    return fifo_tail - fifo_head;
  return fifo_tail + FIFO_SIZE - fifo_head;
}

// Returns the number of bytes currently available in the fifo
int fifo_byte_available() {
  return FIFO_SIZE - 1 - fifo_byte_length();
}

// Returns true if the fifo is empty
int fifo_empty() {
  return fifo_head == fifo_tail;
}

// Return true if the fifo is full
int fifo_full() {
  return fifo_head == ((fifo_tail + 1) % FIFO_SIZE);
}

// Enqueue 1 byte in the fifo if there is space
// returns 1 for sucess and 0 if the fifo was full
int fifo_enqueue(byte b) {
  int new_tail = (fifo_tail + 1) % FIFO_SIZE;
  if (new_tail == fifo_head) return 0; // the fifo is full
  fifo[fifo_tail] = b;
  fifo_tail = new_tail;
  return 1;
}

// Enqueue 1 byte in the fifo without checking for available space
// The caller should have checked that there is enough space
int _fifo_enqueue(byte b) {
  fifo[fifo_tail] = b;
  fifo_tail = (fifo_tail + 1) % FIFO_SIZE;
  return 0; // Dummy added by steffend
}

// Dequeue 1 byte in the fifo.
// The caller should check that the fifo is not empty
byte fifo_dequeue() {
  if (!fifo_empty()) {
    byte result = fifo[fifo_head];
    fifo_head = (fifo_head + 1) % FIFO_SIZE;
    return result;
  }
  return 0;
}

#define timer2_NB_SOFT_TIMER 4
uint32_t timer2_timer[timer2_NB_SOFT_TIMER];
uint32_t  timer2_prev_1sec = 0;



void externalMessageEnqueue(uint8_t * msg, uint8_t msgSize, uint16_t listener_id);

uint8_t timer2_interrupt_counter = 0;
SIGNAL(TIMER2_OVF_vect) {
TCNT2 = 5;
timer2_interrupt_counter++;
if(timer2_interrupt_counter >= 0) {
timer2_interrupt_counter = 0;
}
}



//struct timer2_instance_type {
//    uint16_t listener_id;
//    /*INSTANCE_INFORMATION*/
//} timer2_instance;

struct timer2_instance_type timer2_instance;


void timer2_setup() {
	// Run timer2 interrupt up counting at 250kHz 
		 TCCR2A = 0;
		 TCCR2B = 1<<CS22 | 0<<CS21 | 0<<CS20;
		
		 //Timer2 Overflow Interrupt Enable
		 TIMSK2 |= 1<<TOIE2;


	timer2_prev_1sec = millis() + 1000;
}

void timer2_set_listener_id(uint16_t id) {
	timer2_instance.listener_id = id;
}

void timer2_timer_start(uint8_t id, uint32_t ms) {
if(id <timer2_NB_SOFT_TIMER) {
timer2_timer[id] = ms + millis();
}
}

void timer2_timer_cancel(uint8_t id) {
if(id <timer2_NB_SOFT_TIMER) {
timer2_timer[id] = 0;
}
}

void timer2_timeout(uint8_t id) {
uint8_t enqueue_buf[3];
enqueue_buf[0] = (1 >> 8) & 0xFF;
enqueue_buf[1] = 1 & 0xFF;
enqueue_buf[2] = id;
externalMessageEnqueue(enqueue_buf, 3, timer2_instance.listener_id);
}





void timer2_read() {
    uint32_t tms = millis();
    uint8_t t;
for(t = 0; t < 4; t++) {
if((timer2_timer[t] > 0) && (timer2_timer[t] < tms)) {
timer2_timer[t] = 0;
timer2_timeout(t);
}
}

    if (timer2_prev_1sec < tms) {
        timer2_prev_1sec += 1000;
    }
    
}
// Forwarding of messages timer2::Main::clock::timer_start
void forward_timer2_Main_send_clock_timer_start(struct Main_Instance *_instance, uint8_t id, uint32_t time){
timer2_timer_start(id, time);}

// Forwarding of messages timer2::Main::clock::timer_cancel
void forward_timer2_Main_send_clock_timer_cancel(struct Main_Instance *_instance, uint8_t id){
timer2_timer_cancel(id);}

/*****************************************************************************
 * Implementation for type : Serial
 *****************************************************************************/

// Declaration of prototypes:
//Prototypes: State Machine
void Serial_SerialImpl_OnExit(int state, struct Serial_Instance *_instance);
//Prototypes: Message Sending
void Serial_send_rx_receive_byte(struct Serial_Instance *_instance, uint8_t b);
//Prototypes: Function
// Declaration of functions:

// Sessions functionss:


// On Entry Actions:
void Serial_SerialImpl_OnEntry(int state, struct Serial_Instance *_instance) {
switch(state) {
case SERIAL_SERIALIMPL_STATE:{
_instance->Serial_SerialImpl_State = SERIAL_SERIALIMPL_RECEIVING_STATE;
Serial_SerialImpl_OnEntry(_instance->Serial_SerialImpl_State, _instance);
break;
}
case SERIAL_SERIALIMPL_RECEIVING_STATE:{
Serial.begin(9600);
break;
}
default: break;
}
}

// On Exit Actions:
void Serial_SerialImpl_OnExit(int state, struct Serial_Instance *_instance) {
switch(state) {
case SERIAL_SERIALIMPL_STATE:{
Serial_SerialImpl_OnExit(_instance->Serial_SerialImpl_State, _instance);
break;}
case SERIAL_SERIALIMPL_RECEIVING_STATE:{
break;}
default: break;
}
}

// Event Handlers for incoming messages:
void Serial_handle_tx_write_byte(struct Serial_Instance *_instance, uint8_t b) {
if(!(_instance->active)) return;
//Region SerialImpl
uint8_t Serial_SerialImpl_State_event_consumed = 0;
//End Region SerialImpl
//End dsregion SerialImpl
//Session list: 
if (1) {
Serial.write(b);
Serial_SerialImpl_State_event_consumed = 1;
}
}
int Serial_handle_empty_event(struct Serial_Instance *_instance) {
 uint8_t empty_event_consumed = 0;
if(!(_instance->active)) return 0;
//Region SerialImpl
if (_instance->Serial_SerialImpl_State == SERIAL_SERIALIMPL_RECEIVING_STATE) {
if (Serial.available() > 0) {
Serial_send_rx_receive_byte(_instance, (char)Serial.read());
return 1;
}
}
//begin dispatchEmptyToSession
//end dispatchEmptyToSession
return empty_event_consumed;
}

// Observers for outgoing messages:
void (*external_Serial_send_rx_receive_byte_listener)(struct Serial_Instance *, uint8_t)= 0x0;
void (*Serial_send_rx_receive_byte_listener)(struct Serial_Instance *, uint8_t)= 0x0;
void register_external_Serial_send_rx_receive_byte_listener(void (*_listener)(struct Serial_Instance *, uint8_t)){
external_Serial_send_rx_receive_byte_listener = _listener;
}
void register_Serial_send_rx_receive_byte_listener(void (*_listener)(struct Serial_Instance *, uint8_t)){
Serial_send_rx_receive_byte_listener = _listener;
}
void Serial_send_rx_receive_byte(struct Serial_Instance *_instance, uint8_t b){
if (Serial_send_rx_receive_byte_listener != 0x0) Serial_send_rx_receive_byte_listener(_instance, b);
if (external_Serial_send_rx_receive_byte_listener != 0x0) external_Serial_send_rx_receive_byte_listener(_instance, b);
;
}






/*****************************************************************************
 * Definitions for configuration : MainCfg
 *****************************************************************************/

uint8_t array_main_Main_fgcolor_var[3];
uint8_t array_main_Main_bgcolor_var[3];
//Declaration of instance variables
//Instance uart
// Variables for the properties of the instance
struct Serial_Instance uart_var;
// Variables for the sessions of the instance
//Instance gwcmd
// Variables for the properties of the instance
struct GatewayCommandParser_Instance gwcmd_var;
// Variables for the sessions of the instance
//Instance main
// Variables for the properties of the instance
struct Main_Instance main_var;
// Variables for the sessions of the instance
//Instance disp
// Variables for the properties of the instance
struct DisplayArduino_Instance disp_var;
// Variables for the sessions of the instance


// Enqueue of messages Serial::rx::receive_byte
void enqueue_Serial_send_rx_receive_byte(struct Serial_Instance *_instance, uint8_t b){
if ( fifo_byte_available() > 5 ) {

_fifo_enqueue( (2 >> 8) & 0xFF );
_fifo_enqueue( 2 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_rx >> 8) & 0xFF );
_fifo_enqueue( _instance->id_rx & 0xFF );

// parameter b
union u_b_t {
uint8_t p;
byte bytebuffer[1];
} u_b;
u_b.p = b;
_fifo_enqueue(u_b.bytebuffer[0] & 0xFF );
}
}
// Enqueue of messages GatewayCommandParser::gateway::clear_screen
void enqueue_GatewayCommandParser_send_gateway_clear_screen(struct GatewayCommandParser_Instance *_instance){
if ( fifo_byte_available() > 4 ) {

_fifo_enqueue( (3 >> 8) & 0xFF );
_fifo_enqueue( 3 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_gateway >> 8) & 0xFF );
_fifo_enqueue( _instance->id_gateway & 0xFF );
}
}
// Enqueue of messages GatewayCommandParser::gateway::display_digit
void enqueue_GatewayCommandParser_send_gateway_display_digit(struct GatewayCommandParser_Instance *_instance, int8_t d){
if ( fifo_byte_available() > 5 ) {

_fifo_enqueue( (4 >> 8) & 0xFF );
_fifo_enqueue( 4 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_gateway >> 8) & 0xFF );
_fifo_enqueue( _instance->id_gateway & 0xFF );

// parameter d
union u_d_t {
int8_t p;
byte bytebuffer[1];
} u_d;
u_d.p = d;
_fifo_enqueue(u_d.bytebuffer[0] & 0xFF );
}
}
// Enqueue of messages Main::gateway::button_pushed
void enqueue_Main_send_gateway_button_pushed(struct Main_Instance *_instance){
if ( fifo_byte_available() > 4 ) {

_fifo_enqueue( (5 >> 8) & 0xFF );
_fifo_enqueue( 5 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_gateway >> 8) & 0xFF );
_fifo_enqueue( _instance->id_gateway & 0xFF );
}
}
// Enqueue of messages Main::display::drawInteger
void enqueue_Main_send_display_drawInteger(struct Main_Instance *_instance, uint8_t x, uint8_t y, int16_t v, uint8_t digits, uint8_t scale){
if ( fifo_byte_available() > 10 ) {

_fifo_enqueue( (6 >> 8) & 0xFF );
_fifo_enqueue( 6 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_display >> 8) & 0xFF );
_fifo_enqueue( _instance->id_display & 0xFF );

// parameter x
union u_x_t {
uint8_t p;
byte bytebuffer[1];
} u_x;
u_x.p = x;
_fifo_enqueue(u_x.bytebuffer[0] & 0xFF );

// parameter y
union u_y_t {
uint8_t p;
byte bytebuffer[1];
} u_y;
u_y.p = y;
_fifo_enqueue(u_y.bytebuffer[0] & 0xFF );

// parameter v
union u_v_t {
int16_t p;
byte bytebuffer[2];
} u_v;
u_v.p = v;
_fifo_enqueue(u_v.bytebuffer[0] & 0xFF );
_fifo_enqueue(u_v.bytebuffer[1] & 0xFF );

// parameter digits
union u_digits_t {
uint8_t p;
byte bytebuffer[1];
} u_digits;
u_digits.p = digits;
_fifo_enqueue(u_digits.bytebuffer[0] & 0xFF );

// parameter scale
union u_scale_t {
uint8_t p;
byte bytebuffer[1];
} u_scale;
u_scale.p = scale;
_fifo_enqueue(u_scale.bytebuffer[0] & 0xFF );
}
}
// Enqueue of messages Main::display::create
void enqueue_Main_send_display_create(struct Main_Instance *_instance, uint8_t xsize, uint8_t ysize){
if ( fifo_byte_available() > 6 ) {

_fifo_enqueue( (7 >> 8) & 0xFF );
_fifo_enqueue( 7 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_display >> 8) & 0xFF );
_fifo_enqueue( _instance->id_display & 0xFF );

// parameter xsize
union u_xsize_t {
uint8_t p;
byte bytebuffer[1];
} u_xsize;
u_xsize.p = xsize;
_fifo_enqueue(u_xsize.bytebuffer[0] & 0xFF );

// parameter ysize
union u_ysize_t {
uint8_t p;
byte bytebuffer[1];
} u_ysize;
u_ysize.p = ysize;
_fifo_enqueue(u_ysize.bytebuffer[0] & 0xFF );
}
}
// Enqueue of messages Main::display::update
void enqueue_Main_send_display_update(struct Main_Instance *_instance){
if ( fifo_byte_available() > 4 ) {

_fifo_enqueue( (8 >> 8) & 0xFF );
_fifo_enqueue( 8 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_display >> 8) & 0xFF );
_fifo_enqueue( _instance->id_display & 0xFF );
}
}
// Enqueue of messages Main::display::fillRect
void enqueue_Main_send_display_fillRect(struct Main_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height){
if ( fifo_byte_available() > 8 ) {

_fifo_enqueue( (9 >> 8) & 0xFF );
_fifo_enqueue( 9 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_display >> 8) & 0xFF );
_fifo_enqueue( _instance->id_display & 0xFF );

// parameter x
union u_x_t {
uint8_t p;
byte bytebuffer[1];
} u_x;
u_x.p = x;
_fifo_enqueue(u_x.bytebuffer[0] & 0xFF );

// parameter y
union u_y_t {
uint8_t p;
byte bytebuffer[1];
} u_y;
u_y.p = y;
_fifo_enqueue(u_y.bytebuffer[0] & 0xFF );

// parameter width
union u_width_t {
uint8_t p;
byte bytebuffer[1];
} u_width;
u_width.p = width;
_fifo_enqueue(u_width.bytebuffer[0] & 0xFF );

// parameter height
union u_height_t {
uint8_t p;
byte bytebuffer[1];
} u_height;
u_height.p = height;
_fifo_enqueue(u_height.bytebuffer[0] & 0xFF );
}
}
// Enqueue of messages Main::display::setBGColor
void enqueue_Main_send_display_setBGColor(struct Main_Instance *_instance, uint8_t r, uint8_t g, uint8_t b){
if ( fifo_byte_available() > 7 ) {

_fifo_enqueue( (10 >> 8) & 0xFF );
_fifo_enqueue( 10 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_display >> 8) & 0xFF );
_fifo_enqueue( _instance->id_display & 0xFF );

// parameter r
union u_r_t {
uint8_t p;
byte bytebuffer[1];
} u_r;
u_r.p = r;
_fifo_enqueue(u_r.bytebuffer[0] & 0xFF );

// parameter g
union u_g_t {
uint8_t p;
byte bytebuffer[1];
} u_g;
u_g.p = g;
_fifo_enqueue(u_g.bytebuffer[0] & 0xFF );

// parameter b
union u_b_t {
uint8_t p;
byte bytebuffer[1];
} u_b;
u_b.p = b;
_fifo_enqueue(u_b.bytebuffer[0] & 0xFF );
}
}
// Enqueue of messages Main::display::drawThingML
void enqueue_Main_send_display_drawThingML(struct Main_Instance *_instance, uint8_t x, uint8_t y){
if ( fifo_byte_available() > 6 ) {

_fifo_enqueue( (11 >> 8) & 0xFF );
_fifo_enqueue( 11 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_display >> 8) & 0xFF );
_fifo_enqueue( _instance->id_display & 0xFF );

// parameter x
union u_x_t {
uint8_t p;
byte bytebuffer[1];
} u_x;
u_x.p = x;
_fifo_enqueue(u_x.bytebuffer[0] & 0xFF );

// parameter y
union u_y_t {
uint8_t p;
byte bytebuffer[1];
} u_y;
u_y.p = y;
_fifo_enqueue(u_y.bytebuffer[0] & 0xFF );
}
}
// Enqueue of messages Main::display::setColor
void enqueue_Main_send_display_setColor(struct Main_Instance *_instance, uint8_t r, uint8_t g, uint8_t b){
if ( fifo_byte_available() > 7 ) {

_fifo_enqueue( (12 >> 8) & 0xFF );
_fifo_enqueue( 12 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_display >> 8) & 0xFF );
_fifo_enqueue( _instance->id_display & 0xFF );

// parameter r
union u_r_t {
uint8_t p;
byte bytebuffer[1];
} u_r;
u_r.p = r;
_fifo_enqueue(u_r.bytebuffer[0] & 0xFF );

// parameter g
union u_g_t {
uint8_t p;
byte bytebuffer[1];
} u_g;
u_g.p = g;
_fifo_enqueue(u_g.bytebuffer[0] & 0xFF );

// parameter b
union u_b_t {
uint8_t p;
byte bytebuffer[1];
} u_b;
u_b.p = b;
_fifo_enqueue(u_b.bytebuffer[0] & 0xFF );
}
}
// Enqueue of messages Main::display::destroy
void enqueue_Main_send_display_destroy(struct Main_Instance *_instance){
if ( fifo_byte_available() > 4 ) {

_fifo_enqueue( (13 >> 8) & 0xFF );
_fifo_enqueue( 13 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_display >> 8) & 0xFF );
_fifo_enqueue( _instance->id_display & 0xFF );
}
}
// Enqueue of messages Main::display::clear
void enqueue_Main_send_display_clear(struct Main_Instance *_instance){
if ( fifo_byte_available() > 4 ) {

_fifo_enqueue( (14 >> 8) & 0xFF );
_fifo_enqueue( 14 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_display >> 8) & 0xFF );
_fifo_enqueue( _instance->id_display & 0xFF );
}
}
// Enqueue of messages Main::display::drawRect
void enqueue_Main_send_display_drawRect(struct Main_Instance *_instance, uint8_t x, uint8_t y, uint8_t width, uint8_t height){
if ( fifo_byte_available() > 8 ) {

_fifo_enqueue( (15 >> 8) & 0xFF );
_fifo_enqueue( 15 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_display >> 8) & 0xFF );
_fifo_enqueue( _instance->id_display & 0xFF );

// parameter x
union u_x_t {
uint8_t p;
byte bytebuffer[1];
} u_x;
u_x.p = x;
_fifo_enqueue(u_x.bytebuffer[0] & 0xFF );

// parameter y
union u_y_t {
uint8_t p;
byte bytebuffer[1];
} u_y;
u_y.p = y;
_fifo_enqueue(u_y.bytebuffer[0] & 0xFF );

// parameter width
union u_width_t {
uint8_t p;
byte bytebuffer[1];
} u_width;
u_width.p = width;
_fifo_enqueue(u_width.bytebuffer[0] & 0xFF );

// parameter height
union u_height_t {
uint8_t p;
byte bytebuffer[1];
} u_height;
u_height.p = height;
_fifo_enqueue(u_height.bytebuffer[0] & 0xFF );
}
}
// Enqueue of messages DisplayArduino::display::displayError
void enqueue_DisplayArduino_send_display_displayError(struct DisplayArduino_Instance *_instance){
if ( fifo_byte_available() > 4 ) {

_fifo_enqueue( (16 >> 8) & 0xFF );
_fifo_enqueue( 16 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_display >> 8) & 0xFF );
_fifo_enqueue( _instance->id_display & 0xFF );
}
}
// Enqueue of messages DisplayArduino::display::displayReady
void enqueue_DisplayArduino_send_display_displayReady(struct DisplayArduino_Instance *_instance){
if ( fifo_byte_available() > 4 ) {

_fifo_enqueue( (17 >> 8) & 0xFF );
_fifo_enqueue( 17 & 0xFF );

// ID of the source port of the instance
_fifo_enqueue( (_instance->id_display >> 8) & 0xFF );
_fifo_enqueue( _instance->id_display & 0xFF );
}
}


//New dispatcher for messages
void dispatch_drawInteger(uint16_t sender, uint8_t param_x, uint8_t param_y, int16_t param_v, uint8_t param_digits, uint8_t param_scale) {
if (sender == main_var.id_display) {
DisplayArduino_handle_display_drawInteger(&disp_var, param_x, param_y, param_v, param_digits, param_scale);

}

}


//New dispatcher for messages
void dispatch_button_pushed(uint16_t sender) {
if (sender == main_var.id_gateway) {
GatewayCommandParser_handle_gateway_button_pushed(&gwcmd_var);

}

}


//New dispatcher for messages
void dispatch_fillRect(uint16_t sender, uint8_t param_x, uint8_t param_y, uint8_t param_width, uint8_t param_height) {
if (sender == main_var.id_display) {
DisplayArduino_handle_display_fillRect(&disp_var, param_x, param_y, param_width, param_height);

}

}


//New dispatcher for messages
void dispatch_print_message(uint16_t sender, char * param_msg) {
if (sender == gwcmd_var.id_bletx) {

}

}

void sync_dispatch_GatewayCommandParser_send_bletx_print_message(struct GatewayCommandParser_Instance *_instance, char * msg){
dispatch_print_message(_instance->id_bletx, msg);
}

//New dispatcher for messages
void dispatch_clear_screen(uint16_t sender) {
if (sender == gwcmd_var.id_gateway) {
Main_handle_gateway_clear_screen(&main_var);

}

}


//New dispatcher for messages
void dispatch_receive_byte(uint16_t sender, uint8_t param_b) {
if (sender == uart_var.id_rx) {
GatewayCommandParser_handle_blerx_receive_byte(&gwcmd_var, param_b);

}

}


//New dispatcher for messages
void dispatch_write_byte(uint16_t sender, uint8_t param_b) {
if (sender == gwcmd_var.id_bletx) {
Serial_handle_tx_write_byte(&uart_var, param_b);

}

}

void sync_dispatch_GatewayCommandParser_send_bletx_write_byte(struct GatewayCommandParser_Instance *_instance, uint8_t b){
dispatch_write_byte(_instance->id_bletx, b);
}

//New dispatcher for messages
void dispatch_clear(uint16_t sender) {
if (sender == main_var.id_display) {
DisplayArduino_handle_display_clear(&disp_var);

}

}


//New dispatcher for messages
void dispatch_drawRect(uint16_t sender, uint8_t param_x, uint8_t param_y, uint8_t param_width, uint8_t param_height) {
if (sender == main_var.id_display) {
DisplayArduino_handle_display_drawRect(&disp_var, param_x, param_y, param_width, param_height);

}

}


//New dispatcher for messages
void dispatch_displayError(uint16_t sender) {
if (sender == disp_var.id_display) {

}

}


//New dispatcher for messages
void dispatch_create(uint16_t sender, uint8_t param_xsize, uint8_t param_ysize) {
if (sender == main_var.id_display) {
DisplayArduino_handle_display_create(&disp_var, param_xsize, param_ysize);

}

}


//New dispatcher for messages
void dispatch_update(uint16_t sender) {
if (sender == main_var.id_display) {
DisplayArduino_handle_display_update(&disp_var);

}

}


//New dispatcher for messages
void dispatch_setBGColor(uint16_t sender, uint8_t param_r, uint8_t param_g, uint8_t param_b) {
if (sender == main_var.id_display) {
DisplayArduino_handle_display_setBGColor(&disp_var, param_r, param_g, param_b);

}

}


//New dispatcher for messages
void dispatch_drawThingML(uint16_t sender, uint8_t param_x, uint8_t param_y) {
if (sender == main_var.id_display) {
DisplayArduino_handle_display_drawThingML(&disp_var, param_x, param_y);

}

}


//New dispatcher for messages
void dispatch_setColor(uint16_t sender, uint8_t param_r, uint8_t param_g, uint8_t param_b) {
if (sender == main_var.id_display) {
DisplayArduino_handle_display_setColor(&disp_var, param_r, param_g, param_b);

}

}


//New dispatcher for messages
void dispatch_display_digit(uint16_t sender, int8_t param_d) {
if (sender == gwcmd_var.id_gateway) {
Main_handle_gateway_display_digit(&main_var, param_d);

}

}


//New dispatcher for messages
void dispatch_timer_timeout(uint16_t sender, uint8_t param_id) {
if (sender == timer2_instance.listener_id) {
Main_handle_clock_timer_timeout(&main_var, param_id);

}

}


//New dispatcher for messages
void dispatch_displayReady(uint16_t sender) {
if (sender == disp_var.id_display) {

}

}


//New dispatcher for messages
void dispatch_destroy(uint16_t sender) {
if (sender == main_var.id_display) {
DisplayArduino_handle_display_destroy(&disp_var);

}

}


int processMessageQueue() {
if (fifo_empty()) return 0; // return 0 if there is nothing to do

uint8_t mbufi = 0;

// Read the code of the next port/message in the queue
uint16_t code = fifo_dequeue() << 8;

code += fifo_dequeue();

// Switch to call the appropriate handler
switch(code) {
case 6:{
byte mbuf[10 - 2];
while (mbufi < (10 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_drawInteger = 2;
union u_drawInteger_x_t {
uint8_t p;
byte bytebuffer[1];
} u_drawInteger_x;
u_drawInteger_x.bytebuffer[0] = mbuf[mbufi_drawInteger + 0];
mbufi_drawInteger += 1;
union u_drawInteger_y_t {
uint8_t p;
byte bytebuffer[1];
} u_drawInteger_y;
u_drawInteger_y.bytebuffer[0] = mbuf[mbufi_drawInteger + 0];
mbufi_drawInteger += 1;
union u_drawInteger_v_t {
int16_t p;
byte bytebuffer[2];
} u_drawInteger_v;
u_drawInteger_v.bytebuffer[0] = mbuf[mbufi_drawInteger + 0];
u_drawInteger_v.bytebuffer[1] = mbuf[mbufi_drawInteger + 1];
mbufi_drawInteger += 2;
union u_drawInteger_digits_t {
uint8_t p;
byte bytebuffer[1];
} u_drawInteger_digits;
u_drawInteger_digits.bytebuffer[0] = mbuf[mbufi_drawInteger + 0];
mbufi_drawInteger += 1;
union u_drawInteger_scale_t {
uint8_t p;
byte bytebuffer[1];
} u_drawInteger_scale;
u_drawInteger_scale.bytebuffer[0] = mbuf[mbufi_drawInteger + 0];
mbufi_drawInteger += 1;
dispatch_drawInteger((mbuf[0] << 8) + mbuf[1] /* instance port*/,
 u_drawInteger_x.p /* x */ ,
 u_drawInteger_y.p /* y */ ,
 u_drawInteger_v.p /* v */ ,
 u_drawInteger_digits.p /* digits */ ,
 u_drawInteger_scale.p /* scale */ );
break;
}
case 5:{
byte mbuf[4 - 2];
while (mbufi < (4 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_button_pushed = 2;
dispatch_button_pushed((mbuf[0] << 8) + mbuf[1] /* instance port*/);
break;
}
case 9:{
byte mbuf[8 - 2];
while (mbufi < (8 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_fillRect = 2;
union u_fillRect_x_t {
uint8_t p;
byte bytebuffer[1];
} u_fillRect_x;
u_fillRect_x.bytebuffer[0] = mbuf[mbufi_fillRect + 0];
mbufi_fillRect += 1;
union u_fillRect_y_t {
uint8_t p;
byte bytebuffer[1];
} u_fillRect_y;
u_fillRect_y.bytebuffer[0] = mbuf[mbufi_fillRect + 0];
mbufi_fillRect += 1;
union u_fillRect_width_t {
uint8_t p;
byte bytebuffer[1];
} u_fillRect_width;
u_fillRect_width.bytebuffer[0] = mbuf[mbufi_fillRect + 0];
mbufi_fillRect += 1;
union u_fillRect_height_t {
uint8_t p;
byte bytebuffer[1];
} u_fillRect_height;
u_fillRect_height.bytebuffer[0] = mbuf[mbufi_fillRect + 0];
mbufi_fillRect += 1;
dispatch_fillRect((mbuf[0] << 8) + mbuf[1] /* instance port*/,
 u_fillRect_x.p /* x */ ,
 u_fillRect_y.p /* y */ ,
 u_fillRect_width.p /* width */ ,
 u_fillRect_height.p /* height */ );
break;
}
case 3:{
byte mbuf[4 - 2];
while (mbufi < (4 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_clear_screen = 2;
dispatch_clear_screen((mbuf[0] << 8) + mbuf[1] /* instance port*/);
break;
}
case 2:{
byte mbuf[5 - 2];
while (mbufi < (5 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_receive_byte = 2;
union u_receive_byte_b_t {
uint8_t p;
byte bytebuffer[1];
} u_receive_byte_b;
u_receive_byte_b.bytebuffer[0] = mbuf[mbufi_receive_byte + 0];
mbufi_receive_byte += 1;
dispatch_receive_byte((mbuf[0] << 8) + mbuf[1] /* instance port*/,
 u_receive_byte_b.p /* b */ );
break;
}
case 14:{
byte mbuf[4 - 2];
while (mbufi < (4 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_clear = 2;
dispatch_clear((mbuf[0] << 8) + mbuf[1] /* instance port*/);
break;
}
case 15:{
byte mbuf[8 - 2];
while (mbufi < (8 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_drawRect = 2;
union u_drawRect_x_t {
uint8_t p;
byte bytebuffer[1];
} u_drawRect_x;
u_drawRect_x.bytebuffer[0] = mbuf[mbufi_drawRect + 0];
mbufi_drawRect += 1;
union u_drawRect_y_t {
uint8_t p;
byte bytebuffer[1];
} u_drawRect_y;
u_drawRect_y.bytebuffer[0] = mbuf[mbufi_drawRect + 0];
mbufi_drawRect += 1;
union u_drawRect_width_t {
uint8_t p;
byte bytebuffer[1];
} u_drawRect_width;
u_drawRect_width.bytebuffer[0] = mbuf[mbufi_drawRect + 0];
mbufi_drawRect += 1;
union u_drawRect_height_t {
uint8_t p;
byte bytebuffer[1];
} u_drawRect_height;
u_drawRect_height.bytebuffer[0] = mbuf[mbufi_drawRect + 0];
mbufi_drawRect += 1;
dispatch_drawRect((mbuf[0] << 8) + mbuf[1] /* instance port*/,
 u_drawRect_x.p /* x */ ,
 u_drawRect_y.p /* y */ ,
 u_drawRect_width.p /* width */ ,
 u_drawRect_height.p /* height */ );
break;
}
case 16:{
byte mbuf[4 - 2];
while (mbufi < (4 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_displayError = 2;
dispatch_displayError((mbuf[0] << 8) + mbuf[1] /* instance port*/);
break;
}
case 7:{
byte mbuf[6 - 2];
while (mbufi < (6 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_create = 2;
union u_create_xsize_t {
uint8_t p;
byte bytebuffer[1];
} u_create_xsize;
u_create_xsize.bytebuffer[0] = mbuf[mbufi_create + 0];
mbufi_create += 1;
union u_create_ysize_t {
uint8_t p;
byte bytebuffer[1];
} u_create_ysize;
u_create_ysize.bytebuffer[0] = mbuf[mbufi_create + 0];
mbufi_create += 1;
dispatch_create((mbuf[0] << 8) + mbuf[1] /* instance port*/,
 u_create_xsize.p /* xsize */ ,
 u_create_ysize.p /* ysize */ );
break;
}
case 8:{
byte mbuf[4 - 2];
while (mbufi < (4 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_update = 2;
dispatch_update((mbuf[0] << 8) + mbuf[1] /* instance port*/);
break;
}
case 10:{
byte mbuf[7 - 2];
while (mbufi < (7 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_setBGColor = 2;
union u_setBGColor_r_t {
uint8_t p;
byte bytebuffer[1];
} u_setBGColor_r;
u_setBGColor_r.bytebuffer[0] = mbuf[mbufi_setBGColor + 0];
mbufi_setBGColor += 1;
union u_setBGColor_g_t {
uint8_t p;
byte bytebuffer[1];
} u_setBGColor_g;
u_setBGColor_g.bytebuffer[0] = mbuf[mbufi_setBGColor + 0];
mbufi_setBGColor += 1;
union u_setBGColor_b_t {
uint8_t p;
byte bytebuffer[1];
} u_setBGColor_b;
u_setBGColor_b.bytebuffer[0] = mbuf[mbufi_setBGColor + 0];
mbufi_setBGColor += 1;
dispatch_setBGColor((mbuf[0] << 8) + mbuf[1] /* instance port*/,
 u_setBGColor_r.p /* r */ ,
 u_setBGColor_g.p /* g */ ,
 u_setBGColor_b.p /* b */ );
break;
}
case 11:{
byte mbuf[6 - 2];
while (mbufi < (6 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_drawThingML = 2;
union u_drawThingML_x_t {
uint8_t p;
byte bytebuffer[1];
} u_drawThingML_x;
u_drawThingML_x.bytebuffer[0] = mbuf[mbufi_drawThingML + 0];
mbufi_drawThingML += 1;
union u_drawThingML_y_t {
uint8_t p;
byte bytebuffer[1];
} u_drawThingML_y;
u_drawThingML_y.bytebuffer[0] = mbuf[mbufi_drawThingML + 0];
mbufi_drawThingML += 1;
dispatch_drawThingML((mbuf[0] << 8) + mbuf[1] /* instance port*/,
 u_drawThingML_x.p /* x */ ,
 u_drawThingML_y.p /* y */ );
break;
}
case 12:{
byte mbuf[7 - 2];
while (mbufi < (7 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_setColor = 2;
union u_setColor_r_t {
uint8_t p;
byte bytebuffer[1];
} u_setColor_r;
u_setColor_r.bytebuffer[0] = mbuf[mbufi_setColor + 0];
mbufi_setColor += 1;
union u_setColor_g_t {
uint8_t p;
byte bytebuffer[1];
} u_setColor_g;
u_setColor_g.bytebuffer[0] = mbuf[mbufi_setColor + 0];
mbufi_setColor += 1;
union u_setColor_b_t {
uint8_t p;
byte bytebuffer[1];
} u_setColor_b;
u_setColor_b.bytebuffer[0] = mbuf[mbufi_setColor + 0];
mbufi_setColor += 1;
dispatch_setColor((mbuf[0] << 8) + mbuf[1] /* instance port*/,
 u_setColor_r.p /* r */ ,
 u_setColor_g.p /* g */ ,
 u_setColor_b.p /* b */ );
break;
}
case 4:{
byte mbuf[5 - 2];
while (mbufi < (5 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_display_digit = 2;
union u_display_digit_d_t {
int8_t p;
byte bytebuffer[1];
} u_display_digit_d;
u_display_digit_d.bytebuffer[0] = mbuf[mbufi_display_digit + 0];
mbufi_display_digit += 1;
dispatch_display_digit((mbuf[0] << 8) + mbuf[1] /* instance port*/,
 u_display_digit_d.p /* d */ );
break;
}
case 1:{
byte mbuf[5 - 2];
while (mbufi < (5 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_timer_timeout = 2;
union u_timer_timeout_id_t {
uint8_t p;
byte bytebuffer[1];
} u_timer_timeout_id;
u_timer_timeout_id.bytebuffer[0] = mbuf[mbufi_timer_timeout + 0];
mbufi_timer_timeout += 1;
dispatch_timer_timeout((mbuf[0] << 8) + mbuf[1] /* instance port*/,
 u_timer_timeout_id.p /* id */ );
break;
}
case 17:{
byte mbuf[4 - 2];
while (mbufi < (4 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_displayReady = 2;
dispatch_displayReady((mbuf[0] << 8) + mbuf[1] /* instance port*/);
break;
}
case 13:{
byte mbuf[4 - 2];
while (mbufi < (4 - 2)) mbuf[mbufi++] = fifo_dequeue();
uint8_t mbufi_destroy = 2;
dispatch_destroy((mbuf[0] << 8) + mbuf[1] /* instance port*/);
break;
}
}
return 1;
}

void forward_Main_send_clock_timer_cancel(struct Main_Instance *_instance, uint8_t id){
if(_instance->id_clock == main_var.id_clock) {
forward_timer2_Main_send_clock_timer_cancel(_instance, id);
}
}
void forward_Main_send_clock_timer_start(struct Main_Instance *_instance, uint8_t id, uint32_t time){
if(_instance->id_clock == main_var.id_clock) {
forward_timer2_Main_send_clock_timer_start(_instance, id, time);
}
}

//external Message enqueue
void externalMessageEnqueue(uint8_t * msg, uint8_t msgSize, uint16_t listener_id) {
if ((msgSize >= 2) && (msg != NULL)) {
uint8_t msgSizeOK = 0;
switch(msg[0] * 256 + msg[1]) {
case 1:
if(msgSize == 3) {
msgSizeOK = 1;}
break;
}

if(msgSizeOK == 1) {
if ( fifo_byte_available() > (msgSize + 2) ) {
	uint8_t i;
	for (i = 0; i < 2; i++) {
		_fifo_enqueue(msg[i]);
	}
	_fifo_enqueue((listener_id >> 8) & 0xFF);
	_fifo_enqueue(listener_id & 0xFF);
	for (i = 2; i < msgSize; i++) {
		_fifo_enqueue(msg[i]);
	}
}
}
}
}

void initialize_configuration_MainCfg() {
// Initialize connectors
register_external_Main_send_clock_timer_start_listener(&forward_Main_send_clock_timer_start);
register_external_Main_send_clock_timer_cancel_listener(&forward_Main_send_clock_timer_cancel);
register_Serial_send_rx_receive_byte_listener(&enqueue_Serial_send_rx_receive_byte);
register_GatewayCommandParser_send_bletx_print_message_listener(&sync_dispatch_GatewayCommandParser_send_bletx_print_message);
register_GatewayCommandParser_send_bletx_write_byte_listener(&sync_dispatch_GatewayCommandParser_send_bletx_write_byte);
register_GatewayCommandParser_send_gateway_clear_screen_listener(&enqueue_GatewayCommandParser_send_gateway_clear_screen);
register_GatewayCommandParser_send_gateway_display_digit_listener(&enqueue_GatewayCommandParser_send_gateway_display_digit);
register_Main_send_gateway_button_pushed_listener(&enqueue_Main_send_gateway_button_pushed);
register_Main_send_display_drawInteger_listener(&enqueue_Main_send_display_drawInteger);
register_Main_send_display_create_listener(&enqueue_Main_send_display_create);
register_Main_send_display_update_listener(&enqueue_Main_send_display_update);
register_Main_send_display_fillRect_listener(&enqueue_Main_send_display_fillRect);
register_Main_send_display_setBGColor_listener(&enqueue_Main_send_display_setBGColor);
register_Main_send_display_drawThingML_listener(&enqueue_Main_send_display_drawThingML);
register_Main_send_display_setColor_listener(&enqueue_Main_send_display_setColor);
register_Main_send_display_destroy_listener(&enqueue_Main_send_display_destroy);
register_Main_send_display_clear_listener(&enqueue_Main_send_display_clear);
register_Main_send_display_drawRect_listener(&enqueue_Main_send_display_drawRect);
register_DisplayArduino_send_display_displayError_listener(&enqueue_DisplayArduino_send_display_displayError);
register_DisplayArduino_send_display_displayReady_listener(&enqueue_DisplayArduino_send_display_displayReady);

// Init the ID, state variables and properties for external connector timer2

// Network Initialization

timer2_instance.listener_id = add_instance(&timer2_instance);

timer2_setup();

// End Network Initialization

// Init the ID, state variables and properties for instance uart
uart_var.active = true;
uart_var.id_rx = add_instance( (void*) &uart_var);
uart_var.id_tx = add_instance( (void*) &uart_var);
uart_var.Serial_SerialImpl_State = SERIAL_SERIALIMPL_RECEIVING_STATE;

Serial_SerialImpl_OnEntry(SERIAL_SERIALIMPL_STATE, &uart_var);
// Init the ID, state variables and properties for instance gwcmd
gwcmd_var.active = true;
gwcmd_var.id_blerx = add_instance( (void*) &gwcmd_var);
gwcmd_var.id_bletx = add_instance( (void*) &gwcmd_var);
gwcmd_var.id_gateway = add_instance( (void*) &gwcmd_var);
gwcmd_var.GatewayCommandParser_GatewayCommandParserSC_State = GATEWAYCOMMANDPARSER_GATEWAYCOMMANDPARSERSC_READY_STATE;

GatewayCommandParser_GatewayCommandParserSC_OnEntry(GATEWAYCOMMANDPARSER_GATEWAYCOMMANDPARSERSC_STATE, &gwcmd_var);
// Init the ID, state variables and properties for instance disp
disp_var.active = true;
disp_var.id_display = add_instance( (void*) &disp_var);
disp_var.Display_SC_State = DISPLAY_SC_WAIT_STATE;
disp_var.Display_fg_g_var = 0;
disp_var.DisplayArduino_color_var = ST7735_WHITE;
disp_var.Display_bg_r_var = 0;
disp_var.Display_bg_b_var = 0;
disp_var.Display_bg_g_var = 0;
disp_var.Display_fg_r_var = 0;
disp_var.Display_fg_b_var = 0;

Display_SC_OnEntry(DISPLAY_SC_STATE, &disp_var);
// Init the ID, state variables and properties for instance main
main_var.active = true;
main_var.id_clock = add_instance( (void*) &main_var);
main_var.id_gateway = add_instance( (void*) &main_var);
main_var.id_display = add_instance( (void*) &main_var);
main_var.Main_Main_State = MAIN_MAIN_START_STATE;
main_var.Main_value_var = 64;
main_var.Main_XDISPSIZE_var = 160;
main_var.Main_YDISPSIZE_var = 128;
main_var.Main_fgcolor_var = array_main_Main_fgcolor_var;
main_var.Main_fgcolor_var_size = 3;
main_var.Main_bgcolor_var = array_main_Main_bgcolor_var;
main_var.Main_bgcolor_var_size = 3;

Main_Main_OnEntry(MAIN_MAIN_STATE, &main_var);
}




void setup() {
initialize_configuration_MainCfg();

}

void loop() {

// Network Listener
timer2_read();
// End Network Listener

int emptyEventConsumed = 1;
while (emptyEventConsumed != 0) {
emptyEventConsumed = 0;
emptyEventConsumed += Serial_handle_empty_event(&uart_var);
}

    processMessageQueue();
}
