#include <TM1637Display.h>
#include <Keypad.h>

// Define the pins used by the TM1637 display
#define CLK 2
#define DIO 3

TM1637Display display(CLK, DIO);

// Define the dimensions of the button matrix
const byte ROWS = 4; // four rows
const byte COLS = 4; // four columns

// Define the pins for rows and columns
byte rowPins[ROWS] = {4, 5, 6, 7};
byte colPins[COLS] = {8, 9, 10, 11};

// Define the characters on the buttons
char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

// Initialize the Keypad library
Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

unsigned long previousMillis = 0; // Stores the previous elapsed time
const long interval = 1000; // One second interval
int seconds = 0; // Seconds counter
int minutes = 0; // Minutes counter
bool running = false; // Indicator if the timer is running
bool paused = false; // Indicator if the timer is paused

void setup() {
  Serial.begin(9600); // Initialize serial communication
  display.setBrightness(0x0f); // Set brightness (0x00 to 0x0f)
}

void loop() {
  char key = keypad.getKey();

  if (key != NO_KEY) {
    // Start the timer with the 'A' button
    if (key == 'A') {
      if (!running && !paused) {
        running = true;
        previousMillis = millis(); // Reset the reference time
      }
    }

    // Stop the timer with the 'B' button
    if (key == 'B') {
      if (running) {
        running = false;
        paused = true; // Pause the timer
      }
    }

    // Reset the timer with the 'C' button
    if (key == 'C') {
      running = false;
      paused = false;
      seconds = 0;
      minutes = 0;
      display.showNumberDecEx(0, 0x40, true); // Reset the display to 00:00
    }

    // Resume the timer with the 'D' button
    if (key == 'D') {
      if (!running && paused) {
        running = true;
        paused = false;
        previousMillis = millis(); // Reset the reference time
      }
    }
  }

  // Update the timer
  if (running) {
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      seconds++;

      if (seconds == 60) {
        seconds = 0;
        minutes++;
      }

      if (minutes == 100) { // If the counter exceeds 99:59, reset it
        minutes = 0;
      }

      // Display the time on the display
      int displayTime = (minutes * 100) + seconds; // Convert the time to MMSS format
      display.showNumberDecEx(displayTime, 0x40, true); // Display MM:SS with a blinking colon

      // Send the timer information via serial communication
      Serial.print("Minutes: ");
      Serial.print(minutes);
      Serial.print(" Seconds: ");
      Serial.println(seconds);
    }
  }
}
