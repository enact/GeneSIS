# Transforming a Raspberry Pi into a Router

To deploy, it is necessary that the various components such as your machine, Raspberry Pis, or others are on the same network. To address this issue, I transformed one of the Raspberry Pis into a router so that the other Raspberry Pis and my machine can connect to it.

I will explain the procedure I followed to perform this manipulation.

Here are the details of the different Raspberry Pis:

### Raspberry Pi Router (Configured)

User: pi  
Password: raspberry  
MAC Address: b8:27:eb:fb:a1:22

### Other Raspberry Pis (Configured)

User: rayane  
Password: 1234  
MAC Address: d8:3a:dd:c7:b9:68   

User: rayane2  
Password: 1234  
MAC Address: d8:3a:dd:c7:b9:c2

User: rayane3  
Password: 1234  
MAC Address: d8:3a:dd:c7:b4:dc

User: rayane4  
Password: 1234  
MAC Address: d8:3a:dd:c7:b9:56

You just need to plug them in and the 4 Raspberry Pis should automatically connect to the Raspberry Pi Router.

In the next [section](#first-make-sure-the-distribution-is-up-to-date-using-the-following-3-commands), I will explain the procedure I followed to set up the above-mentioned network configuration.

## Perform this manipulation on the Raspberry Pi that you want to transform into a router.

The following procedure allows you to configure a Raspberry Pi as a router to use it as a Wi-Fi access point for devices that wish to connect. It will also share its internet connection so that connected devices can access the internet.

### First, make sure the distribution is up to date using the following 3 commands:

```
sudo apt-get update  
sudo apt-get full-upgrade  
sudo reboot
```

These commands update, upgrade if necessary, and reboot the system.

### Next, we need to configure a static IP address for the network interface. 
### Edit the file /etc/dhcpcd.conf to add the following lines:

```
interface wlan0  
static ip_address=192.168.4.1/24  
nohook wpa_supplicant  
```

The line interface wlan0 specifies that the following settings will apply to the wireless network interface named wlan0. The line static ip_address=192.168.4.1/24 will assign a static IP address to the wlan0 interface, meaning that the interface will use this fixed IP address instead of obtaining an address via the Dynamic Host Configuration Protocol (DHCP). Finally, the last line indicates that the use of the wpa_supplicant service (which is responsible for managing secure Wi-Fi connections) should not be used for this interface and thus disables it. 

### Next, in the Raspberry Pi terminal, execute this command:

```
sudo apt install dnsmasq hostapd  
```

This command installs the packages [dnsmasq](https://doc.ubuntu-fr.org/configuration_serveur_dns_dhcp) and [hostapd](https://doc.ubuntu-fr.org/hostapd) on the system. The tool dnsmasq is a lightweight DNS, DHCP, and TFTP server. The tool hostapd is software that allows a wireless network card to become a Wi-Fi access point.

### Then, we need to configure DHCP and DNS with dnsmasq.

To do this, we modify the file /etc/dnsmasq.conf to add the following lines: 

```
interface=wlan0  
bind-dynamic  
domain-needed  
bogus-priv  
dhcp-range=192.168.4.100,192.168.4.200,255.255.255.0,24h  
```

Here, the IP addresses assigned will be in the range of 192.168.4.100 to 192.168.4.200 with a subnet mask of 255.255.255.0. It will dynamically assign interfaces and adjust them as needed.

### In this fifth step, we need to configure the access point hostapd.

To do this, we modify the file /etc/hostapd/hostapd.conf and add the corresponding lines:

```
interface=wlan0  
driver=nl80211  
ssid=NetworkName
hw_mode=g  
channel=7  
wmm_enabled=0  
macaddr_acl=0  
auth_algs=1  
ignore_broadcast_ssid=0  
wpa=2  
wpa_passphrase=Password
wpa_key_mgmt=WPA-PSK  
wpa_pairwise=TKIP  
rsn_pairwise=CCMP  
```

This configuration is for setting up the Wi-Fi access point. You can change the password or the network name.

### Next, we need to enable IP routing.

Simply uncomment the line ```net.ipv4.ip_forward=1``` in the file /etc/sysctl.conf.  

### Add a masquerading rule to share the internet connection:

```
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE  
sudo sh -c "iptables-save > /etc/iptables.ipv4.nat  
```

These commands allow devices connected to the Raspberry Pi's Wi-Fi to access the internet via the Raspberry Pi's Ethernet connection. 

### Finally, we add:
```
iptables-restore < /etc/iptables.ipv4.nat
```
to the file /etc/rc.local so that the iptables rules are automatically applied.

Then, ```sudo reboot``` the Raspberry Pi to apply the changes.

If you have followed these steps correctly, upon startup, the different Raspberry Pis should connect to the relevant network!